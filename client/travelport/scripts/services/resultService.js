'use strict';

// defines functions that can be called from controllers and directives
app.factory('resultService', function ($http) {

	var server = "http://localhost/galileo2/server/web/app_dev.php/api/galileo/results/create";

	var serviceFunctions = {

		post: function (results) {
			$http.post(server,results).then(function(response){
				console.log("SUCCESS",response);
				return response;
			},function(error){
				console.log("ERROR",error);
				return error;
			});
		}
	}

	return serviceFunctions;
});