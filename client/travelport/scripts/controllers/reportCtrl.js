'use strict';

var app = angular.module('app');

app.controller('reportCtrl', ['$scope', 'resultService',function ($scope,resultService) {

   $scope.agence = function(begin,end){
     window.location.href = "http://93.115.97.235/travelport/galileohelpdesk/admin/web/app_dev.php/api/galileo/results/agency?begin=" + begin + "&end=" +  end;
   }


   $scope.town = function(begin,end){
     window.location.href = "http://93.115.97.235/travelport/galileohelpdesk/admin/web/app_dev.php/api/galileo/results/town?begin=" + begin + "&end=" +  end;
   }

    $scope.company = function(begin,end){
     window.location.href = "http://93.115.97.235/travelport/galileohelpdesk/admin/web/app_dev.php/api/galileo/results/company?begin=" + begin + "&end=" +  end;
   }

    $scope.analyse = function(begin,end,pcc){
     window.location.href = "http://93.115.97.235/travelport/galileohelpdesk/admin/web/app_dev.php/api/galileo/results/report?begin=" + begin + "&end=" +  end + "&pcc=" +  pcc ;
   }

    $scope.solde = function(login,password){
     window.location.href = "http://93.115.97.235/gtwmoney/admin/web/app_dev.php/api/galileo/money/solde?login=" + login + "&password=" +  password;
   }

    $scope.history = function(login,password){
     window.location.href = "http://93.115.97.235/gtwmoney/admin/web/app_dev.php/api/galileo/money/booking?login=" + login + "&password=" +  password ;
   }



}]);
