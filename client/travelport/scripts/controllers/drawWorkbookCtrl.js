'use strict';

//var app = angular.module('app');

app.controller('drawWorkbookCtrl', ['$scope', '$http',function ($scope,$http) {

    $scope.workbook = null;
    $scope.sheetIndex = -1;
    $scope.results = [];
    var server = "http://localhost/galileohelpdesk/admin/web/app_dev.php/api/galileo/results/create";


    /*$scope.progressbar = ngProgressFactory.createInstance();

    $scope.color = 'firebrick';
    $scope.height = '7px';
    $scope.contained_progressbar = ngProgressFactory.createInstance();
    $scope.contained_progressbar.setParent(document.getElementById('demo_contained'));
    $scope.contained_progressbar.setAbsolute(); */

    $scope.ama = 0;

    $scope.gal = 0;

    $scope.sab = 0;

    $scope.total = 1;

    $scope.jours = 7;

    $scope.progressTotal = 1;
    $scope.progressCurrent = 0;
    $scope.tables = [];

    document.getElementById('importFile').addEventListener('change', function () {
        loadWorkbook();
    });

    function loadWorkbook() {
        var reader = new FileReader(),
            fileData;

        reader.onload = function (e) {
        	//$scope.workbook = wijmo.xlsx.XlsxConverter.import(reader.result);
        	var workbook = new wijmo.xlsx.Workbook();
        	workbook.load(reader.result);
        	$scope.workbook = workbook;
            $scope.drawSheet($scope.workbook.activeWorksheet || 0);
            if (!$scope.$root.$$phase) {
                $scope.$apply();
            }
            
        };
        var file = document.getElementById('importFile').files[0];
        if (file) {
        	//reader.readAsArrayBuffer(file);
        	reader.readAsDataURL(file);
        }
    }

    $scope.calculate = function(n){

        $scope.jours = n;
        var c = 0;
        var operator = {
            code : "",
            name : ""
        };
        var agency = {
            name: ""
        };
        var company = {
            code : "",
            name : ""
        };
        var date;
        var segment = 0;
        for(var i = 6; i < 8000 ; i++){

            if($scope.workbook.sheets[0].rows[i]){
                if($scope.workbook.sheets[0].rows[i].cells[0]){
                   if($scope.workbook.sheets[0].rows[i].cells[0].value != ''){
                     operator.code = $scope.workbook.sheets[0].rows[i].cells[0].value;
                  } 
                }

                if($scope.workbook.sheets[0].rows[i].cells[2]){
                   if($scope.workbook.sheets[0].rows[i].cells[2].value != ''){
                     agency.name = $scope.workbook.sheets[0].rows[i].cells[2].value;
                  } 
                }

                if($scope.workbook.sheets[0].rows[i].cells[3]){
                   if($scope.workbook.sheets[0].rows[i].cells[3].value != ''){
                     company.code = $scope.workbook.sheets[0].rows[i].cells[3].value;
                     company.name = $scope.workbook.sheets[0].rows[i].cells[4].value;

                  } 
                }  


            }
            
 
              for(var l = 5 ; l < $scope.jours + 5; l++){
              
                if($scope.workbook.sheets[0].rows[i]){
                   if($scope.workbook.sheets[0].rows[i].cells[l]){
                      date = $scope.workbook.sheets[0].rows[5].cells[l].value + "T0:00:00P";
                      segment = $scope.workbook.sheets[0].rows[i].cells[l].value;
                      if(segment == ''){
                        segment = 0;
                      }
                      console.log($scope.workbook.sheets[0].rows[i].cells[1].value);
                      var result = {
                         operator:{
                            code : operator.code
                         },
                         agency : {
                            name: agency.name,
                            town: {
                                code : $scope.workbook.sheets[0].rows[i].cells[1].value,
                                name : ""
                            }

                         },
                         company : {
                            code : company.code,
                            name : company.name
                         },
                         date : date,
                         segment : segment
                      };

                      if(result.segment != 0){
                          $scope.results.push(result);
                      }
                  } 
                }
                  
              }

                  
           }

            console.log($scope.results);

         //   resultService.post($scope.results);




    }


    var timer = new Timer();


    timer.addEventListener('secondsUpdated', function (e) {
        $scope.hours = timer.getTotalTimeValues().hours;
        $scope.minutes = timer.getTotalTimeValues().minutes;
        $scope.seconds = timer.getTotalTimeValues().seconds;
        $scope.leftTime = 0;
    });
    

    $scope.calculatecode = function(n){
        $scope.jours = n;
        alert($scope.jours);
        timer.start({precision: 'seconds'});
        var c = 0;
        $scope.tables = [];
        $scope.results = [];
        var operator = {
            code : "",
            name : ""
        };
        var agency = {
            name: "",
            codegal: ""
        };
        var company = {
            code : "",
            name : ""
        };
        var date;
        var segment = 0;
        for(var i = 6; i < 8000 ; i++){

            if($scope.workbook.sheets[0].rows[i]){
                if($scope.workbook.sheets[0].rows[i].cells[0]){
                   if($scope.workbook.sheets[0].rows[i].cells[0].value != ''){
                     operator.code = $scope.workbook.sheets[0].rows[i].cells[0].value;
                  } 
                }

                if($scope.workbook.sheets[0].rows[i].cells[2]){
                   if($scope.workbook.sheets[0].rows[i].cells[2].value != ''){
                     agency.name = $scope.workbook.sheets[0].rows[i].cells[2].value;
                     console.log($scope.workbook.sheets[0].rows[i].cells[1].value);
                     agency.codegal = $scope.workbook.sheets[0].rows[i].cells[1].value;
                  } 
                }

                if($scope.workbook.sheets[0].rows[i].cells[3]){
                   if($scope.workbook.sheets[0].rows[i].cells[3].value != ''){
                     company.code = $scope.workbook.sheets[0].rows[i].cells[3].value;
                     company.name = $scope.workbook.sheets[0].rows[i].cells[4].value;

                  } 
                }  


            }
            
              $scope.begin = $scope.workbook.sheets[0].rows[5].cells[5].value;
              var k = $scope.jours + 4;
              $scope.end = $scope.workbook.sheets[0].rows[5].cells[k].value;

              for(var l = 5 ; l <= k; l++){
                if($scope.workbook.sheets[0].rows[i]){
                   if($scope.workbook.sheets[0].rows[i].cells[l]){
                      date = $scope.workbook.sheets[0].rows[5].cells[l].value + "T0:00:00P";
                      segment = $scope.workbook.sheets[0].rows[i].cells[l].value;
                      if(segment == ''){
                        segment = 0;
                      }
                      var result = {
                         operator:{
                            code : operator.code
                         },
                         agency : {
                            name: agency.name,
                            town: {
                                code : "",
                                name : ""
                            },
                            codegal : agency.codegal
                         },
                         company : {
                            code : company.code,
                            name : company.name
                         },
                         date : date,
                         segment : segment
                      };

                      if(result.segment != 0){
                          $scope.results.push(result);
                      }
                  } 
                }
                  
               }

                  
            }

          //console.log($scope.results);
          $scope.progressTotal = $scope.results.length;

          var i,j,temparray,chunk = 150;
          
          for (i=0,j=$scope.results.length; i<j; i+=chunk) {
              temparray = $scope.results.slice(i,i+chunk);
              // do whatever
              $scope.tables.push(temparray);
          }

          console.log($scope.tables);
          $scope.post(0);

    }


    $scope.post = function(n){
      /*resultService.post($scope.tables[n]).then(function(response){
          console.log(response);
          if(n+1 <= $scope.tables.length){
            $scope.post(n+1);
          }
      });*/
      $http.post(server,$scope.tables[n]).then(function(response){
        console.log("SUCCESS",response);
        $scope.progressCurrent  = $scope.progressCurrent  + $scope.tables[n].length;
        $scope.leftTime = Math.round(((($scope.progressTotal * parseInt($scope.seconds)) / $scope.progressCurrent) - parseInt($scope.seconds))/60);
         if($scope.progressCurrent  < $scope.progressTotal){
            $scope.post(n+1);
          }else{
            timer.stop();
          }
      },function(error){
        //$scope.progressCurrent  = $scope.progressCurrent  + $scope.tables[n].length;
        //$scope.leftTime = Math.round(((($scope.progressTotal * parseInt($scope.seconds)) / $scope.progressCurrent) - parseInt($scope.seconds))/60);
        $scope.post(n);
         
      });
    }

    $scope.drawSheet = function (sheetIndex) {
        var drawRoot = document.getElementById('tableHost');
        drawRoot.textContent = '';
        $scope.sheetIndex = sheetIndex;
        xlsxImport.drawWorksheet($scope.workbook, sheetIndex, drawRoot, 10200, 100);
    }

}]);
