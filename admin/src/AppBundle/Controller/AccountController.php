<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Doctrine\ORM\QueryBuilder;
use CoreBundle\Exception\RessourceValidationException;
use FOS\RestBundle\Controller\FOSRestController;
use AppBundle\Service\AccountService;
use AppBundle\Entity\User;

class AccountController extends Controller
{



    private $accountService;

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
    * @Rest\Get("/api/account/login")
    * @Rest\QueryParam(
    *     name="login",
    *     nullable=false,
    *     description="Email for logged"
    * )
    * @Rest\QueryParam(
    *     name="password",
    *     nullable=false,
    *     description="Password"
    * )
    * )
    * @Rest\View()
    */
    public function loginAction(ParamFetcherInterface $paramFetcher)
    {
      $login = $paramFetcher->get('login');
      $password = $paramFetcher->get('password');
      $this->accountService = $this->get('app.service.account');
      $result = $this->accountService->login($login,$password);
      $data = $this->get('jms_serializer')->serialize($result, 'json');
      $response = new Response($data);
      $response->headers->set('Content-Type', 'application/json');
      return $response;
    }

    /**
    * @Rest\Post("/api/account/register")
    * @Rest\View(StatusCode = 201))
    * @ParamConverter("user", converter="fos_rest.request_body")
    */
    public function registerAction(User $user)
    {
         $this->accountService = $this->get('app.service.account');
         $result = $this->accountService->register($user);
         $data = $this->get('jms_serializer')->serialize($result, 'json');
         $response = new Response($data);
         $response->headers->set('Content-Type', 'application/json');
         return $response;
    }

    /**
    * @Rest\Post("/api/account/update")
    * @Rest\View(StatusCode = 201))
    * @ParamConverter("user", converter="fos_rest.request_body")
    */
    public function updateAction(User $user)
    {
         $this->accountService = $this->get('app.service.account');
         $result = $this->accountService->update($user);
         $data = $this->get('jms_serializer')->serialize($result, 'json');
         $response = new Response($data);
         $response->headers->set('Content-Type', 'application/json');
         return $response;
    }

     /**
    * @Rest\Get("/api/account/actived")
    * @Rest\QueryParam(
    *     name="login",
    *     nullable=false,
    *     description="Email for logged"
    * )
    * @Rest\View()
    */
    public function activedAction(ParamFetcherInterface $paramFetcher)
    {
      $login = $paramFetcher->get('login');
      $this->accountService = $this->get('app.service.account');
      $result = $this->accountService->activedUser($login);
      $data = $this->get('jms_serializer')->serialize($result, 'json');
      $response = new Response($data);
      $response->headers->set('Content-Type', 'application/json');
      return $response;
    }

     /**
    * @Rest\Get("/api/account/desactived")
    * @Rest\QueryParam(
    *     name="login",
    *     nullable=false,
    *     description="Email for logged"
    * )
    * @Rest\View()
    */
    public function desactivedAction(ParamFetcherInterface $paramFetcher)
    {
      $login = $paramFetcher->get('login');
      $this->accountService = $this->get('app.service.account');
      $result = $this->accountService->desactivedUser($login);
      $data = $this->get('jms_serializer')->serialize($result, 'json');
      $response = new Response($data);
      $response->headers->set('Content-Type', 'application/json');
      return $response;
    }

     /**
    * @Rest\Get("/api/account/account")
    * @Rest\QueryParam(
    *     name="login",
    *     nullable=false,
    *     description="Email for logged"
    * )
    * @Rest\View()
    */
    public function accountAction(ParamFetcherInterface $paramFetcher)
    {
      $login = $paramFetcher->get('login');
      $this->accountService = $this->get('app.service.account');
      $result = $this->accountService->findAccount($login);
      $data = $this->get('jms_serializer')->serialize($result, 'json');
      $response = new Response($data);
      $response->headers->set('Content-Type', 'application/json');
      return $response;
    }




}
