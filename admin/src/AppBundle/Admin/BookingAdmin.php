<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Travelport\GalileoBundle\Service\ContactService;
use Travelport\GalileoBundle\Entity\Contact;




class BookingAdmin extends AbstractAdmin
{

    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('pnr', 'text')->add('nbSegment', 'number')->add('description', 'text')->add('lieu')->add('date', 'date')
        ->add('agency', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\Agency',
            'property' => 'name',
        ))
        ->add('agent', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\Contact',
            'property' => 'pseudo',
        ))
        ->add('agencyiata', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\Agency',
            'property' => 'name',
        ))
        ->add('company', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\Company',
            'property' => 'code',
        ))
        ;
    }

    public function prePersist($booking)
    {

       if($booking->getAgent()->getActivate()){
          $old = $booking->getAgent()->getSolde();
          $value = $booking->getAgent()->getValue();
          $nb = $booking->getNbSegment();
          $booking->getAgent()->setSolde($old + $value * $nb);
       } 
      

    } 

    public function preRemove($booking)
    {
       if($booking->getAgent()->getActivate()){
          $old = $booking->getAgent()->getSolde();
          $value = $booking->getAgent()->getValue();
          $nb = $booking->getNbSegment();
          $booking->getAgent()->setSolde($old - $value * $nb);
       } 

    } 


   


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('pnr')->add('description')->add('date')->add('agency.name')->add('agent.pseudo')->add('agencyiata.name')->add('company.code');

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('pnr')->add('description')->add('date')->add('agency.name')->add('agent.name')->add('agencyiata.name')->add('company.code')->add('nbSegment')->add('_action', null, array(
            'actions' => array(
                'delete' => array(),
            )
        )); 
    }
}