<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class TeamAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name','text')->add('number','text')->add('email','text')->add('role','text')->add('disponibility')
        ->add('town', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\Town',
            'property' => 'name',
        ));
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name')
                        ->add('number')
                        ->add('email')
                        ->add('role')
                        ->add('disponibility')
                        ->add('town.name');

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('name')->add('email')->add('number')->add('role')->add('disponibility')->add('town.name')->add('_action', null, array(
            'actions' => array(
                'show' => array(),
                'edit' => array(),
                'delete' => array(),
            )
        ));
    }
}