<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class TransactionAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('designation', 'text')->add('description', 'text')->add('date','date')->add('somme','number')
        ->add('agent', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\Contact',
            'property' => 'name',
        ))
        ;
    }

     public function prePersist($transaction)
    {
       $old = $transaction->getAgent()->getSolde();
       $cost = $transaction->getSomme();
       $transaction->getAgent()->setSolde($old + $cost);

    } 

    public function preRemove($transaction)
    {
       $old = $transaction->getAgent()->getSolde();
       $cost = $transaction->getSomme();
       $transaction->getAgent()->setSolde($old - $cost);

    } 

    public function getExportFields()
    {
        return array('designation', 'description', 'date', 'agent.name','somme');
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('designation')->add('description')->add('date')->add('agent.name')->add('somme');

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('designation')->add('description')->add('date')->add('agent.name')->add('somme')->add('_action', null, array(
            'actions' => array(
                'edit' => array(),
                'delete' => array(),
            )
        ));
    }
}