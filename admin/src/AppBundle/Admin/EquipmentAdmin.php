<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class EquipmentAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('serial')->add('marque')->add('model')->add('dateAchat','date')->add('description','textarea')
        ->add('type', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\TypeEquipment',
            'property' => 'name',
        ))
        ->add('agency', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\Agency',
            'property' => 'codegal',
        ))
         ->add('fournisseur', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\Fournisseur',
            'property' => 'name',
        ));
    }

     public function getExportFields()
    {
        return array('agency.name','type.name', 'serial', 'marque','model','description','fournisseur.name');
    }



    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('serial')->add('marque')->add('model')->add('agency.name')->add('agency.town.code')->add('type.name')->add('fournisseur.name')->add('description');

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('type.name')->add('serial')->add('marque')->add('model')->add('agency.name')->add('fournisseur.name')->add('_action', null, array(
            'actions' => array(
                'show' => array(),
                'edit' => array(),
                'delete' => array(),
            )
        ));
    }
}