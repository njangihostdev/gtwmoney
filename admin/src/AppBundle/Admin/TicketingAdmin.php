<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Travelport\GalileoBundle\Service\TeamService;
use Travelport\GalileoBundle\Service\TicketingService;



class TicketingAdmin extends AbstractAdmin
{



    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name')->add('number')->add('email')->add('clientDescription','textarea')->add('formalDescription','textarea')
        ->add('state', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\StateTicketing',
            'property' => 'name',
        ))
        ->add('type', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\TypeTicketing',
            'property' => 'name',
        ))
        ->add('helpdesk', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\Team',
            'property' => 'name',
        ))
        ->add('technician', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\Team',
            'property' => 'name',
        ))
        ->add('agency', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\Agency',
            'property' => 'codegal',
        ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name')->add('number')->add('email')->add('agency.name')->add('type.name')->add('clientDescription')->add('formalDescription')->add('helpdesk.name')->add('technician.name')->add('state.name');

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('name')->add('createdDate')->add('assignDate')->add('closeDate')->add('agency.name')->add('type.name')->add('clientDescription')->add('helpdesk.name')->add('technician.name')->add('state.name')->add('satisfaction')->add('_action', null, array(
            'actions' => array(
                'show' => array(),
                'edit' => array(),
                'delete' => array(),
            )
        ));
    }
}