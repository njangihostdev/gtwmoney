<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ResultAdmin extends AbstractAdmin
{

   

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('date', 'date')->add('segment', 'number')
        ->add('operator', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\Operator',
            'property' => 'name',
        ))
        ->add('agency', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\Agency',
            'property' => 'name',
        ))
        ->add('company', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\Company',
            'property' => 'name',
        ));
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('date','doctrine_orm_date_range')
                        ->add('segment')
                        ->add('operator.code')
                        ->add('agency.name')
                        ->add('company.code')
                        ->add('agency.town.code');

    }

    public function getExportFields()
    {
        return array('date','agency.name','company.name', 'operator.code', 'agency.town.code','segment');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('date')->add('segment')->add('operator.code')->add('agency.name')->add('company.name')->add('agency.town.code');
    }

    public function getTemplate($name)
    {
            if($name == 'list') {
                return 'AppBundle:admin:list.html.twig';
            }

            return parent::getTemplate($name); 
            //stub
    }

public function getSumOf($field)
{
    $datagrid = $this->getDatagrid();
    $datagrid->buildPager();
    $query = $datagrid->getQuery();

    $query->select('SUM( ' . $query->getRootAlias() . '.' . $field . ') as total');
    $query->setFirstResult(null);
    $query->setMaxResults(null);


    $result = $query->execute(array(), \Doctrine\ORM\Query::HYDRATE_SINGLE_SCALAR);
    
    return $result;
}



}