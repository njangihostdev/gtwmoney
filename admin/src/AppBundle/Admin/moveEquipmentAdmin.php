<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class moveEquipmentAdmin extends AbstractAdmin
{

     public function prePersist($moveEquipment)
    {
        $moveEquipment->setAgencyBegin($moveEquipment->getEquipments()->getAgency());
    }

     public function preUpdate($moveEquipment)
    {
        $moveEquipment->setAgencyBegin($moveEquipment->getEquipments()->getAgency());
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('print', $this->getRouterIdParameter().'/print');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('description','textarea')
       
        ->add('technician', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\Team',
            'property' => 'name',
            'label' => 'Agent Galileo',
        ))


        ->add('equipments', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\Equipment',
            'property' => 'serial',
        ))


        ->add('agencyEnd', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\Agency',
            'property' => 'codegal',
            'label' => 'Agence Cible'
        ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('createdDate')->add('confirmDate')->add('agencyEnd.name')->add('description')->add('state')->add('technician.name');

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('createdDate')->add('confirmDate')->add('agencyBegin.name',null,array('label' => 'Agence de Depart'))->add('agencyEnd.name',null,array('label' => 'Agence Cible'))->add('equipments.type.name')->add('state',null,array('label' => 'Confirmer'))->add('technician.name',null,array('label' => 'Agent Galileo'))->add('_action', null, array(
            'actions' => array(
                'delete' => array(),
                'print' => array(
                    'template' => 'AppBundle:CRUD:print.html.twig'
                )
            )
        ));
    }
}