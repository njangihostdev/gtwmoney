<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class AgencyAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', 'text')->add('locality', 'text')->add('iata')
        ->add('header', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\Team',
            'property' => 'name',
        ))
        ->add('town', 'sonata_type_model', array(
            'class' => 'Travelport\GalileoBundle\Entity\Town',
            'property' => 'name',
        ))
        ;
    }

    public function getExportFields()
    {
        return array('name', 'locality', 'town.code', 'header.name','iata');
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name')->add('town.code')->add('locality')->add('header.name')->add('iata');

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('name')->add('town.code')->add('locality')->add('iata')->add('header.name')->add('_action', null, array(
            'actions' => array(
                'edit' => array(),
                'delete' => array(),
            )
        ));
    }
}