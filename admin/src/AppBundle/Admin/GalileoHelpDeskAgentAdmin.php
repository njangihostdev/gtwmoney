<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class GalileoHelpDeskAgentAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', 'text')->add('surname', 'text')->add('telephone','text')->add('email','text');
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name')->add('town.code')->add('codeama')->add('codegal')->add('codesab');

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('name')->add('town.code')->add('codeama')->add('codegal')->add('codesab')->add('_action', null, array(
            'actions' => array(
                'show' => array(),
                'edit' => array(),
                'delete' => array(),
            )
        ));
    }
}