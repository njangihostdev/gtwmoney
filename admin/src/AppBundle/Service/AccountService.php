<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use AppBundle\Entity\Role;
use AppBundle\Classes\Error;
use AppBundle\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;



class AccountService
{

  private $request;
  private $em;

  public function __construct(EntityManager $entityManager)
  {
    $this->request = new Request();
    $this->em = $entityManager;
  }

  public function register(User $user)
  {


    $userFind = null;

    try {          
      $userFind = $this->em->getRepository("AppBundle:User")->findByEmail($user->getEmail());
    } catch (\PDOException $e) {
       $userFind = null;
    }

     if($userFind==null){
        $user->setActived(false);
        $role = $this->em->getRepository("AppBundle:Role")->findOneByCode("ROLE_USER"); 
        $user->addRole($role);
        $this->em->persist($user);
        $this->em->flush();
        return $user;
     }else{
        $error = new Error();
        $error->message = "this email already use";
        $error->code = 500;
        return $error;
     }
     
  }

  public function activedUser($login)
  {
    $user = $this->em->getRepository("AppBundle:User")->findOneByEmail($login);
    $user->setActived(true);
    $this->em->flush();
    return $user;
  }

  public function desactivedUser($login)
  {
    $user = $this->em->getRepository("AppBundle:User")->findOneByEmail($login);
    $user->setActived(false);
    $this->em->flush();
    return $user;
  }

  public function update(User $user)
  {
    $userLast = $this->em->getRepository("AppBundle:User")->find($user->getId());
    $userLast->setName($user->getName());
    $userLast->setSurname($user->getSurname());
    $userLast->setEmail($user->getEmail());
    $userLast->setPassword($user->getPassword());
    $this->em->flush();
    return $user;
  }


  public function findAccount($login){
        $user = new User();
        $user = $this->em->getRepository("AppBundle:User")->findByEmail($login); 
        return $user;
  }

  public function findRole($code){
        $role = $this->em->getRepository("AppBundle:User")->findOneByCode($code); 
        return $role;
  }

  public function login($login,$password){
      $user = new User();
      $user = $this->em->getRepository("AppBundle:User")->findOneByEmail($login);
      if($user==null OR $user->getPassword() != $password){
         $error = new Error();
          $error->message = "this account don't exist";
          $error->code = 500;
          return $error;
      }else{
        if($user->getActived() AND $user->getPassword() == $password){
          return $user;
        }else{
          $error = new Error();
          $error->message = "this account is desactived";
          $error->code = 500;
          return $error;
        }
      }   
  }

}
