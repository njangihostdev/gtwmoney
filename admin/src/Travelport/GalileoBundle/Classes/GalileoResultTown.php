<?php

namespace Travelport\GalileoBundle\Classes;
use Travelport\GalileoBundle\Entity\Town;


class GalileoResultTown
{
   public $town;

   public $ama;

   public $gal;

   public $sab;

   public function __construct(){
     $this->agency = new Town();
     $this->ama = 0;
     $this->gal = 0;
     $this->sab = 0;
   }
}

