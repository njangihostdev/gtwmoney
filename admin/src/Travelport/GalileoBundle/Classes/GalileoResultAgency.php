<?php

namespace Travelport\GalileoBundle\Classes;
use Travelport\GalileoBundle\Entity\Agency;


class GalileoResultAgency
{
   public $agency;

   public $operator;

   public $ama;

   public $gal;

   public $sab;

   public function __construct(){
     $this->agency = new Agency();
     $this->ama = 0;
     $this->gal = 0;
     $this->sab = 0;
   }
}

