<?php

namespace Travelport\GalileoBundle\Classes;
use Travelport\GalileoBundle\Entity\Company;


class GalileoResultCompany
{
   public $company;

   public $ama;

   public $gal;

   public $sab;

   public function __construct(){
     $this->company = new Company();
     $this->ama = 0;
     $this->gal = 0;
     $this->sab = 0;
   }
}

