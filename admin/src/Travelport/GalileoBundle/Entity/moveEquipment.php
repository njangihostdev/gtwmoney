<?php

namespace Travelport\GalileoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * moveEquipment
 *
 * @ORM\Table(name="move_equipment")
 * @ORM\Entity(repositoryClass="Travelport\GalileoBundle\Repository\moveEquipmentRepository")
 */
class moveEquipment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=5000)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="datetime")
     */
    private $createdDate;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="confirmDate", type="datetime")
     */
    private $confirmDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="state", type="boolean")
     */
    private $state;


     /**
     * @ORM\ManyToOne(targetEntity="Team",  inversedBy="moveEquipments")
    */
    private $technician;


     /**
     * @ORM\ManyToOne(targetEntity="Agency",  cascade="persist")
    */
    private $agencyBegin;

     /**
     * @ORM\ManyToOne(targetEntity="Agency",  cascade="persist")
    */
    private $agencyEnd;


    /**
     * @ORM\ManyToOne(targetEntity="Equipment", inversedBy="moves")
    */
    private $equipments;


    public function __construct(){
        $this->state = false;
        $this->createdDate  = new \Datetime();
        $this->confirmDate  = new \Datetime();

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return moveEquipment
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return moveEquipment
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set state
     *
     * @param boolean $state
     *
     * @return moveEquipment
     */
    public function setState($state)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return boolean
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set technician
     *
     * @param \Travelport\GalileoBundle\Entity\Team $technician
     *
     * @return moveEquipment
     */
    public function setTechnician(\Travelport\GalileoBundle\Entity\Team $technician = null)
    {
        $this->technician = $technician;
    
        return $this;
    }

    /**
     * Get technician
     *
     * @return \Travelport\GalileoBundle\Entity\Team
     */
    public function getTechnician()
    {
        return $this->technician;
    }

    /**
     * Set agencyBegin
     *
     * @param \Travelport\GalileoBundle\Entity\Agency $agencyBegin
     *
     * @return moveEquipment
     */
    public function setAgencyBegin(\Travelport\GalileoBundle\Entity\Agency $agencyBegin = null)
    {
        $this->agencyBegin = $agencyBegin;
    
        return $this;
    }

    /**
     * Get agencyBegin
     *
     * @return \Travelport\GalileoBundle\Entity\Agency
     */
    public function getAgencyBegin()
    {
        return $this->agencyBegin;
    }

    /**
     * Set agencyEnd
     *
     * @param \Travelport\GalileoBundle\Entity\Agency $agencyEnd
     *
     * @return moveEquipment
     */
    public function setAgencyEnd(\Travelport\GalileoBundle\Entity\Agency $agencyEnd = null)
    {
        $this->agencyEnd = $agencyEnd;
    
        return $this;
    }

    /**
     * Get agencyEnd
     *
     * @return \Travelport\GalileoBundle\Entity\Agency
     */
    public function getAgencyEnd()
    {
        return $this->agencyEnd;
    }
    
   

    /**
     * Set equipments
     *
     * @param \Travelport\GalileoBundle\Entity\Equipment $equipments
     *
     * @return moveEquipment
     */
    public function setEquipments(\Travelport\GalileoBundle\Entity\Equipment $equipments = null)
    {
        $this->equipments = $equipments;
    
        return $this;
    }

    /**
     * Get equipments
     *
     * @return \Travelport\GalileoBundle\Entity\Equipment
     */
    public function getEquipments()
    {
        return $this->equipments;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return moveEquipment
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    
        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set confirmDate
     *
     * @param \DateTime $confirmDate
     *
     * @return moveEquipment
     */
    public function setConfirmDate($confirmDate)
    {
        $this->confirmDate = $confirmDate;
    
        return $this;
    }

    /**
     * Get confirmDate
     *
     * @return \DateTime
     */
    public function getConfirmDate()
    {
        return $this->confirmDate;
    }
}
