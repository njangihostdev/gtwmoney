<?php

namespace Travelport\GalileoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;


/**
 * StateTicketing
 *
 * @ORM\Table(name="state_ticketing")
 * @ORM\Entity(repositoryClass="Travelport\GalileoBundle\Repository\StateTicketingRepository")
 * @Serializer\ExclusionPolicy("ALL")
 */
class StateTicketing
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Serializer\Expose
     */
    private $name;

     /**
     * @ORM\OneToMany(targetEntity="Ticketing", cascade="persist", mappedBy="state")
    */
    private $ticketings;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return StateTicketing
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}

