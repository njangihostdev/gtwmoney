<?php

namespace Travelport\GalileoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fournisseur
 *
 * @ORM\Table(name="fournisseur")
 * @ORM\Entity(repositoryClass="Travelport\GalileoBundle\Repository\FournisseurRepository")
 */
class Fournisseur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="ncontr", type="string", length=255)
     */
    private $ncontr;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=255)
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="siegesocial", type="string", length=255)
     */
    private $siegesocial;

    /**
     * @ORM\OneToMany(targetEntity="Equipment", cascade="persist", mappedBy="fournisseur")
    */
    private $equipments;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Fournisseur
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set ncontr
     *
     * @param string $ncontr
     *
     * @return Fournisseur
     */
    public function setNcontr($ncontr)
    {
        $this->ncontr = $ncontr;

        return $this;
    }

    /**
     * Get ncontr
     *
     * @return string
     */
    public function getNcontr()
    {
        return $this->ncontr;
    }

    /**
     * Set contact
     *
     * @param string $contact
     *
     * @return Fournisseur
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set siegesocial
     *
     * @param string $siegesocial
     *
     * @return Fournisseur
     */
    public function setSiegesocial($siegesocial)
    {
        $this->siegesocial = $siegesocial;

        return $this;
    }

    /**
     * Get siegesocial
     *
     * @return string
     */
    public function getSiegesocial()
    {
        return $this->siegesocial;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->equipments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add equipment
     *
     * @param \Travelport\GalileoBundle\Entity\Equipment $equipment
     *
     * @return Fournisseur
     */
    public function addEquipment(\Travelport\GalileoBundle\Entity\Equipment $equipment)
    {
        $this->equipments[] = $equipment;

        return $this;
    }

    /**
     * Remove equipment
     *
     * @param \Travelport\GalileoBundle\Entity\Equipment $equipment
     */
    public function removeEquipment(\Travelport\GalileoBundle\Entity\Equipment $equipment)
    {
        $this->equipments->removeElement($equipment);
    }

    /**
     * Get equipments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEquipments()
    {
        return $this->equipments;
    }
}
