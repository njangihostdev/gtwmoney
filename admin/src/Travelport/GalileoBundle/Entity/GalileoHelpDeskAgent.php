<?php

namespace Travelport\GalileoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GalileoHelpDeskAgent
 *
 * @ORM\Table(name="galileo_help_desk_agent")
 * @ORM\Entity(repositoryClass="Travelport\GalileoBundle\Repository\GalileoHelpDeskAgentRepository")
 */
class GalileoHelpDeskAgent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255)
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=255)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity="Agency", mappedBy="agentHelpdesk")
    */
    private $agencies;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return GalileoHelpDeskAgent
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return GalileoHelpDeskAgent
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    
        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return GalileoHelpDeskAgent
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    
        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return GalileoHelpDeskAgent
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->agencies = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add agency
     *
     * @param \Travelport\GalileoBundle\Entity\Agency $agency
     *
     * @return GalileoHelpDeskAgent
     */
    public function addAgency(\Travelport\GalileoBundle\Entity\Agency $agency)
    {
        $this->agencies[] = $agency;
    
        return $this;
    }

    /**
     * Remove agency
     *
     * @param \Travelport\GalileoBundle\Entity\Agency $agency
     */
    public function removeAgency(\Travelport\GalileoBundle\Entity\Agency $agency)
    {
        $this->agencies->removeElement($agency);
    }

    /**
     * Get agencies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAgencies()
    {
        return $this->agencies;
    }
}
