<?php

namespace Travelport\GalileoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Travelport\GalileoBundle\Service\StateTicketingService;
use JMS\Serializer\Annotation as Serializer;


/**
 * Ticketing
 *
 * @ORM\Table(name="ticketing")
 * @ORM\Entity(repositoryClass="Travelport\GalileoBundle\Repository\TicketingRepository")
 * @Serializer\ExclusionPolicy("ALL")
 */
class Ticketing
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose
     */
    private $id;

     /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Serializer\Expose
     */
    private $name;

     /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=100)
     * @Serializer\Expose
     */
    private $number;


    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100)
     * @Serializer\Expose
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="clientDescription", type="string", length=5000)
     * @Serializer\Expose
     */
    private $clientDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="formalDescription", type="string", length=5000)
     * @Serializer\Expose
     */
    private $formalDescription;

    /**
     * @var boolean
     *
     * @ORM\Column(name="satisfaction", type="boolean")
     */
    private $satisfaction;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="datetime")
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="assignDate", type="datetime")
     */
    private $assignDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="closeDate", type="datetime")
     */
    private $closeDate;

    /**
     * @ORM\ManyToOne(targetEntity="Agency",  inversedBy="ticketings")
     * @Serializer\Expose
    */
    private $agency;

   /**
     * @ORM\ManyToOne(targetEntity="TypeTicketing",  inversedBy="ticketings")
     * @Serializer\Expose
    */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="Team",  inversedBy="ticketings")
     * @Serializer\Expose
    */
    private $technician;

     /**
     * @ORM\ManyToOne(targetEntity="Team",  inversedBy="ticketings")
     * @Serializer\Expose
    */
    private $helpdesk;

    /**
     * @ORM\ManyToOne(targetEntity="StateTicketing",  inversedBy="ticketings")
     * @Serializer\Expose
    */
    private $state;


    /**
     * @ORM\ManyToOne(targetEntity="Contact",  inversedBy="ticketings")
     * @Serializer\Expose
    */
    private $agent;
    

    public function __construct()
    {
        $this->createdDate = new \DateTime();
        //$stateTicketingService = new StateTicketingService();
        //$this->state = $stateTicketingService->get(1);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clientDescription
     *
     * @param string $clientDescription
     *
     * @return Ticketing
     */
    public function setClientDescription($clientDescription)
    {
        $this->clientDescription = $clientDescription;
    
        return $this;
    }

    /**
     * Get clientDescription
     *
     * @return string
     */
    public function getClientDescription()
    {
        return $this->clientDescription;
    }

    /**
     * Set formalDescription
     *
     * @param string $formalDescription
     *
     * @return Ticketing
     */
    public function setFormalDescription($formalDescription)
    {
        $this->formalDescription = $formalDescription;
    
        return $this;
    }

    /**
     * Get formalDescription
     *
     * @return string
     */
    public function getFormalDescription()
    {
        return $this->formalDescription;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return Ticketing
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    
        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set agency
     *
     * @param \Travelport\GalileoBundle\Entity\Agency $agency
     *
     * @return Ticketing
     */
    public function setAgency(\Travelport\GalileoBundle\Entity\Agency $agency = null)
    {
        $this->agency = $agency;
    
        return $this;
    }

    /**
     * Get agency
     *
     * @return \Travelport\GalileoBundle\Entity\Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * Set type
     *
     * @param \Travelport\GalileoBundle\Entity\TypeTicketing $type
     *
     * @return Ticketing
     */
    public function setType(\Travelport\GalileoBundle\Entity\TypeTicketing $type = null)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return \Travelport\GalileoBundle\Entity\TypeTicketing
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set technician
     *
     * @param \Travelport\GalileoBundle\Entity\Team $technician
     *
     * @return Ticketing
     */
    public function setTechnician(\Travelport\GalileoBundle\Entity\Team $technician = null)
    {
        $this->technician = $technician;
    
        return $this;
    }

    /**
     * Get technician
     *
     * @return \Travelport\GalileoBundle\Entity\Team
     */
    public function getTechnician()
    {
        return $this->technician;
    }

    /**
     * Set state
     *
     * @param \Travelport\GalileoBundle\Entity\StateTicketing $state
     *
     * @return Ticketing
     */
    public function setState(\Travelport\GalileoBundle\Entity\StateTicketing $state = null)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return \Travelport\GalileoBundle\Entity\StateTicketing
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set satisfaction
     *
     * @param boolean $satisfaction   
     *
     * @return Ticketing
     */
    public function setSatisfaction($satisfaction)
    {
        $this->satisfaction = $satisfaction;
    
        return $this;
    }

    /**
     * Get satisfaction
     *
     * @return boolean
     */
    public function getSatisfaction()
    {
        return $this->satisfaction;
    }

    /**
     * Set helpdesk
     *
     * @param \Travelport\GalileoBundle\Entity\Team $helpdesk
     *
     * @return Ticketing
     */
    public function setHelpdesk(\Travelport\GalileoBundle\Entity\Team $helpdesk = null)
    {
        $this->helpdesk = $helpdesk;
    
        return $this;
    }

    /**
     * Get helpdesk
     *
     * @return \Travelport\GalileoBundle\Entity\Team
     */
    public function getHelpdesk()
    {
        return $this->helpdesk;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Ticketing
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Ticketing
     */
    public function setNumber($number)
    {
        $this->number = $number;
    
        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Ticketing
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set assignDate
     *
     * @param \DateTime $assignDate
     *
     * @return Ticketing
     */
    public function setAssignDate($assignDate)
    {
        $this->assignDate = $assignDate;
    
        return $this;
    }

    /**
     * Get assignDate
     *
     * @return \DateTime
     */
    public function getAssignDate()
    {
        return $this->assignDate;
    }

    /**
     * Set closeDate
     *
     * @param \DateTime $closeDate
     *
     * @return Ticketing
     */
    public function setCloseDate($closeDate)
    {
        $this->closeDate = $closeDate;
    
        return $this;
    }

    /**
     * Get closeDate
     *
     * @return \DateTime
     */
    public function getCloseDate()
    {
        return $this->closeDate;
    }

    /**
     * Set agent
     *
     * @param \Travelport\GalileoBundle\Entity\Contact $agent
     *
     * @return Ticketing
     */
    public function setAgent(\Travelport\GalileoBundle\Entity\Contact $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \Travelport\GalileoBundle\Entity\Contact
     */
    public function getAgent()
    {
        return $this->agent;
    }
}
