<?php

namespace Travelport\GalileoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;


/**
 * TypeTicketing
 *
 * @ORM\Table(name="type_ticketing")
 * @ORM\Entity(repositoryClass="Travelport\GalileoBundle\Repository\TypeTicketingRepository")
 * @Serializer\ExclusionPolicy("ALL")
 */
class TypeTicketing
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Serializer\Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=5000)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="Ticketing", cascade="persist", mappedBy="type")
    */
    private $ticketings;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return TypeTicketing
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return TypeTicketing
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ticketings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add ticketing
     *
     * @param \Travelport\GalileoBundle\Entity\Ticketing $ticketing
     *
     * @return TypeTicketing
     */
    public function addTicketing(\Travelport\GalileoBundle\Entity\Ticketing $ticketing)
    {
        $this->ticketings[] = $ticketing;
    
        return $this;
    }

    /**
     * Remove ticketing
     *
     * @param \Travelport\GalileoBundle\Entity\Ticketing $ticketing
     */
    public function removeTicketing(\Travelport\GalileoBundle\Entity\Ticketing $ticketing)
    {
        $this->ticketings->removeElement($ticketing);
    }

    /**
     * Get ticketings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTicketings()
    {
        return $this->ticketings;
    }
}
