<?php

namespace Travelport\GalileoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Contact
 *
 * @ORM\Table(name="contact")
 * @ORM\Entity(repositoryClass="Travelport\GalileoBundle\Repository\ContactRepository")
 * @Serializer\ExclusionPolicy("ALL")
 */
class Contact
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Serializer\Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="contacts", type="string", length=5000)
     * @Serializer\Expose
     */
    private $contacts;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Serializer\Expose
     */
    private $email;


    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;


    /**
     * @var string
     *
     * @ORM\Column(name="pseudo", type="string", length=255)
     */
    private $pseudo;


     /**
     * @var string
     *
     * @ORM\Column(name="parrain", type="string", length=255)
     */
    private $parrain;


    /**
     * @var integer
     *
     * @ORM\Column(name="solde", type="integer")
     */
    private $solde;


     /**
     * @var integer
     *
     * @ORM\Column(name="value", type="integer")
     */
    private $value;


      /**
     * @var boolean
     *
     * @ORM\Column(name="activate", type="boolean")
     */
    private $activate;

     /**
     * @ORM\ManyToOne(targetEntity="Agency",  inversedBy="contacts")
     * @Serializer\Expose
    */
    private $agency;


    /**
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="agent", cascade="persist" )
    */
    private $bookings;


    /**
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="agentiata", cascade="persist" )
    */
    private $bookingsiata;


    /**
     * @ORM\OneToMany(targetEntity="Transaction", mappedBy="agent", cascade="persist" )
    */
    private $transactions;



     /**
     * @ORM\OneToMany(targetEntity="Ticketing", cascade="persist", mappedBy="agent")
    */
    private $ticketings;

    public function __construct(){
        $this->activate = false;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Contact
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set contacts
     *
     * @param string $contacts
     *
     * @return Contact
     */
    public function setContacts($contacts)
    {
        $this->contacts = $contacts;
    
        return $this;
    }

    /**
     * Get contacts
     *
     * @return string
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set agency
     *
     * @param \Travelport\GalileoBundle\Entity\Agency $agency
     *
     * @return Contact
     */
    public function setAgency(\Travelport\GalileoBundle\Entity\Agency $agency = null)
    {
        $this->agency = $agency;
    
        return $this;
    }

    /**
     * Get agency
     *
     * @return \Travelport\GalileoBundle\Entity\Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * Set activate
     *
     * @param boolean $activate
     *
     * @return Contact
     */
    public function setActivate($activate)
    {
        $this->activate = $activate;

        return $this;
    }

    /**
     * Get activate
     *
     * @return boolean
     */
    public function getActivate()
    {
        return $this->activate;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Contact
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Add ticketing
     *
     * @param \Travelport\GalileoBundle\Entity\Ticketing $ticketing
     *
     * @return Contact
     */
    public function addTicketing(\Travelport\GalileoBundle\Entity\Ticketing $ticketing)
    {
        $this->ticketings[] = $ticketing;

        return $this;
    }

    /**
     * Remove ticketing
     *
     * @param \Travelport\GalileoBundle\Entity\Ticketing $ticketing
     */
    public function removeTicketing(\Travelport\GalileoBundle\Entity\Ticketing $ticketing)
    {
        $this->ticketings->removeElement($ticketing);
    }

    /**
     * Get ticketings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTicketings()
    {
        return $this->ticketings;
    }

    /**
     * Set pseudo
     *
     * @param string $pseudo
     *
     * @return Contact
     */
    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    /**
     * Get pseudo
     *
     * @return string
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * Set parrain
     *
     * @param string $parrain
     *
     * @return Contact
     */
    public function setParrain($parrain)
    {
        $this->parrain = $parrain;

        return $this;
    }

    /**
     * Get parrain
     *
     * @return string
     */
    public function getParrain()
    {
        return $this->parrain;
    }

    /**
     * Set solde
     *
     * @param integer $solde
     *
     * @return Contact
     */
    public function setSolde($solde)
    {
        $this->solde = $solde;

        return $this;
    }

    /**
     * Get solde
     *
     * @return integer
     */
    public function getSolde()
    {
        return $this->solde;
    }

    /**
     * Add booking
     *
     * @param \Travelport\GalileoBundle\Entity\Booking $booking
     *
     * @return Contact
     */
    public function addBooking(\Travelport\GalileoBundle\Entity\Booking $booking)
    {
        $this->bookings[] = $booking;

        return $this;
    }

    /**
     * Remove booking
     *
     * @param \Travelport\GalileoBundle\Entity\Booking $booking
     */
    public function removeBooking(\Travelport\GalileoBundle\Entity\Booking $booking)
    {
        $this->bookings->removeElement($booking);
    }

    /**
     * Get bookings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookings()
    {
        return $this->bookings;
    }

    /**
     * Add bookingsIATum
     *
     * @param \Travelport\GalileoBundle\Entity\Booking $bookingsIATum
     *
     * @return Contact
     */
    public function addBookingsIATum(\Travelport\GalileoBundle\Entity\Booking $bookingsIATum)
    {
        $this->bookingsIATA[] = $bookingsIATum;

        return $this;
    }

    /**
     * Remove bookingsIATum
     *
     * @param \Travelport\GalileoBundle\Entity\Booking $bookingsIATum
     */
    public function removeBookingsIATum(\Travelport\GalileoBundle\Entity\Booking $bookingsIATum)
    {
        $this->bookingsIATA->removeElement($bookingsIATum);
    }

    /**
     * Get bookingsIATA
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookingsIATA()
    {
        return $this->bookingsIATA;
    }

    /**
     * Set value
     *
     * @param integer $value
     *
     * @return Contact
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return integer
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Add transaction
     *
     * @param \Travelport\GalileoBundle\Entity\Transaction $transaction
     *
     * @return Contact
     */
    public function addTransaction(\Travelport\GalileoBundle\Entity\Transaction $transaction)
    {
        $this->transactions[] = $transaction;

        return $this;
    }

    /**
     * Remove transaction
     *
     * @param \Travelport\GalileoBundle\Entity\Transaction $transaction
     */
    public function removeTransaction(\Travelport\GalileoBundle\Entity\Transaction $transaction)
    {
        $this->transactions->removeElement($transaction);
    }

    /**
     * Get transactions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransactions()
    {
        return $this->transactions;
    }
}
