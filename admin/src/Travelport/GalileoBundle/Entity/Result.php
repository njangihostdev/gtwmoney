<?php

namespace Travelport\GalileoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Result
 *
 * @ORM\Table(name="result")
 * @ORM\Entity(repositoryClass="Travelport\GalileoBundle\Repository\ResultRepository")
 */
class Result
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @ORM\ManyToOne(targetEntity="Operator", inversedBy="results", cascade="persist" )
    */
    private $operator;

    /**
     * @ORM\ManyToOne(targetEntity="Agency", inversedBy="results", cascade="persist")
    */
    private $agency;

    /**
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="results", cascade="persist")
    */
    private $company;

    /**
     * @var \Date
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(name="segment", type="integer")
     */
    private $segment;


    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Result
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set segment
     *
     * @param integer $segment
     *
     * @return Result
     */
    public function setSegment($segment)
    {
        $this->segment = $segment;
    
        return $this;
    }

    /**
     * Get segment
     *
     * @return integer
     */
    public function getSegment()
    {
        return $this->segment;
    }

    /**
     * Set operator
     *
     * @param \Travelport\GalileoBundle\Entity\Operator $operator
     *
     * @return Result
     */
    public function setOperator(\Travelport\GalileoBundle\Entity\Operator $operator = null)
    {
        $this->operator = $operator;
    
        return $this;
    }

    /**
     * Get operator
     *
     * @return \Travelport\GalileoBundle\Entity\Operator
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * Set agency
     *
     * @param \Travelport\GalileoBundle\Entity\Agency $agency
     *
     * @return Result
     */
    public function setAgency(\Travelport\GalileoBundle\Entity\Agency $agency = null)
    {
        $this->agency = $agency;
    
        return $this;
    }

    /**
     * Get agency
     *
     * @return \Travelport\GalileoBundle\Entity\Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * Set company
     *
     * @param \Travelport\GalileoBundle\Entity\Company $company
     *
     * @return Result
     */
    public function setCompany(\Travelport\GalileoBundle\Entity\Company $company = null)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     *
     * @return \Travelport\GalileoBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }
}
