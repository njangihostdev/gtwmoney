<?php

namespace Travelport\GalileoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Equipment
 *
 * @ORM\Table(name="equipment")
 * @ORM\Entity(repositoryClass="Travelport\GalileoBundle\Repository\EquipmentRepository")
 */
class Equipment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="serial", type="string", length=255)
     */
    private $serial;

    /**
     * @var string
     *
     * @ORM\Column(name="marque", type="string", length=255)
     */
    private $marque;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=255)
     */
    private $model;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=5000)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="TypeEquipment",  inversedBy="equipments")
    */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="Agency",  inversedBy="equipments")
    */
    private $agency;

    /**
     * @ORM\ManyToOne(targetEntity="Fournisseur",  inversedBy="equipments")
    */
    private $fournisseur;

    /**
     * @ORM\OneToMany(targetEntity="moveEquipment",  mappedBy="equipments")
    */
    private $moves;

     /**
     * @var \Date
     *
     * @ORM\Column(name="dateAchat", type="date")
     */
    private $dateAchat;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set serial
     *
     * @param string $serial
     *
     * @return Equipment
     */
    public function setSerial($serial)
    {
        $this->serial = $serial;
    
        return $this;
    }

    /**
     * Get serial
     *
     * @return string
     */
    public function getSerial()
    {
        return $this->serial;
    }

    /**
     * Set marque
     *
     * @param string $marque
     *
     * @return Equipment
     */
    public function setMarque($marque)
    {
        $this->marque = $marque;
    
        return $this;
    }

    /**
     * Get marque
     *
     * @return string
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * Set model
     *
     * @param string $model
     *
     * @return Equipment
     */
    public function setModel($model)
    {
        $this->model = $model;
    
        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Equipment
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @param \Travelport\GalileoBundle\Entity\TypeEquipment $type
     *
     * @return Equipment
     */
    public function setType(\Travelport\GalileoBundle\Entity\TypeEquipment $type = null)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return \Travelport\GalileoBundle\Entity\TypeEquipment
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set agency
     *
     * @param \Travelport\GalileoBundle\Entity\Agency $agency
     *
     * @return Equipment
     */
    public function setAgency(\Travelport\GalileoBundle\Entity\Agency $agency = null)
    {
        $this->agency = $agency;
    
        return $this;
    }

    /**
     * Get agency
     *
     * @return \Travelport\GalileoBundle\Entity\Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->moves = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add move
     *
     * @param \Travelport\GalileoBundle\Entity\moveEquipment $move
     *
     * @return Equipment
     */
    public function addMove(\Travelport\GalileoBundle\Entity\moveEquipment $move)
    {
        $this->moves[] = $move;
    
        return $this;
    }

    /**
     * Remove move
     *
     * @param \Travelport\GalileoBundle\Entity\moveEquipment $move
     */
    public function removeMove(\Travelport\GalileoBundle\Entity\moveEquipment $move)
    {
        $this->moves->removeElement($move);
    }

    /**
     * Get moves
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMoves()
    {
        return $this->moves;
    }

    /**
     * Set fournisseur
     *
     * @param \Travelport\GalileoBundle\Entity\Fournisseur $fournisseur
     *
     * @return Equipment
     */
    public function setFournisseur(\Travelport\GalileoBundle\Entity\Fournisseur $fournisseur = null)
    {
        $this->fournisseur = $fournisseur;

        return $this;
    }

    /**
     * Get fournisseur
     *
     * @return \Travelport\GalileoBundle\Entity\Fournisseur
     */
    public function getFournisseur()
    {
        return $this->fournisseur;
    }

    /**
     * Set dateAchat
     *
     * @param \DateTime $dateAchat
     *
     * @return Equipment
     */
    public function setDateAchat($dateAchat)
    {
        $this->dateAchat = $dateAchat;

        return $this;
    }

    /**
     * Get dateAchat
     *
     * @return \DateTime
     */
    public function getDateAchat()
    {
        return $this->dateAchat;
    }
}
