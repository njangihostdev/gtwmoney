<?php

namespace Travelport\GalileoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Company
 *
 * @ORM\Table(name="company")
 * @ORM\Entity(repositoryClass="Travelport\GalileoBundle\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\OneToMany(targetEntity="Result", mappedBy="company", cascade="persist" )
    */
    private $results;

    /**
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="company", cascade="persist" )
    */
    private $bookings;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, unique=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="datetimetz")
     */
    private $createdDate;

    /**
     * @var string
     *
     * @ORM\Column(name="createdBy", type="string", length=255)
     */
    private $createdBy;

    public function __construct(){
        $this->createdDate  = new \Datetime();
        $this->createdBy = "Simo Tresor";
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Company
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return Company
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    
        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     *
     * @return Company
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Add agency
     *
     * @param \Travelport\GalileoBundle\Entity\Agency $agency
     *
     * @return Company
     */
    public function addAgency(\Travelport\GalileoBundle\Entity\Agency $agency)
    {
        $this->agencies[] = $agency;
    
        return $this;
    }

    /**
     * Remove agency
     *
     * @param \Travelport\GalileoBundle\Entity\Agency $agency
     */
    public function removeAgency(\Travelport\GalileoBundle\Entity\Agency $agency)
    {
        $this->agencies->removeElement($agency);
    }

    /**
     * Get agencies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAgencies()
    {
        return $this->agencies;
    }

    /**
     * Add result
     *
     * @param \Travelport\GalileoBundle\Entity\Result $result
     *
     * @return Company
     */
    public function addResult(\Travelport\GalileoBundle\Entity\Result $result)
    {
        $this->results[] = $result;
    
        return $this;
    }

    /**
     * Remove result
     *
     * @param \Travelport\GalileoBundle\Entity\Result $result
     */
    public function removeResult(\Travelport\GalileoBundle\Entity\Result $result)
    {
        $this->results->removeElement($result);
    }

    /**
     * Get results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * Add booking
     *
     * @param \Travelport\GalileoBundle\Entity\Booking $booking
     *
     * @return Company
     */
    public function addBooking(\Travelport\GalileoBundle\Entity\Booking $booking)
    {
        $this->bookings[] = $booking;

        return $this;
    }

    /**
     * Remove booking
     *
     * @param \Travelport\GalileoBundle\Entity\Booking $booking
     */
    public function removeBooking(\Travelport\GalileoBundle\Entity\Booking $booking)
    {
        $this->bookings->removeElement($booking);
    }

    /**
     * Get bookings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookings()
    {
        return $this->bookings;
    }
}
