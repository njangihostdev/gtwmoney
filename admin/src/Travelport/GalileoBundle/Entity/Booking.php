<?php

namespace Travelport\GalileoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;


/**
 * Booking
 *
 * @ORM\Table(name="booking")
 * @ORM\Entity(repositoryClass="Travelport\GalileoBundle\Repository\BookingRepository")
 */
class Booking
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="PNR", type="string", length=6)
     */
    private $pnr;

    /**
     * @var int
     *
     * @ORM\Column(name="nbSegment", type="integer")
     */
    private $nbSegment;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="lieu", type="boolean")
     */
    private $lieu;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;


     /**
     * @ORM\ManyToOne(targetEntity="Agency",  inversedBy="bookings")
    */
    private $agency;


      /**
     * @ORM\ManyToOne(targetEntity="Agency",  inversedBy="bookingiata")
    */
    private $agencyiata;



     /**
     * @ORM\ManyToOne(targetEntity="Company",  inversedBy="bookings")
    */
    private $company;


     /**
     * @ORM\ManyToOne(targetEntity="Contact",  inversedBy="bookings")
    */
    private $agent;




    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pnr
     *
     * @param string $pnr
     *
     * @return Booking
     */
    public function setPnr($pnr)
    {
        $this->pnr = $pnr;

        return $this;
    }

    /**
     * Get pnr
     *
     * @return string
     */
    public function getPnr()
    {
        return $this->pnr;
    }

    /**
     * Set nbSegment
     *
     * @param integer $nbSegment
     *
     * @return Booking
     */
    public function setNbSegment($nbSegment)
    {
        $this->nbSegment = $nbSegment;

        return $this;
    }

    /**
     * Get nbSegment
     *
     * @return integer
     */
    public function getNbSegment()
    {
        return $this->nbSegment;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Booking
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set lieu
     *
     * @param boolean $lieu
     *
     * @return Booking
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return boolean
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Booking
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set agency
     *
     * @param \Travelport\GalileoBundle\Entity\Agency $agency
     *
     * @return Booking
     */
    public function setAgency(\Travelport\GalileoBundle\Entity\Agency $agency = null)
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get agency
     *
     * @return \Travelport\GalileoBundle\Entity\Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * Set agencyiata
     *
     * @param \Travelport\GalileoBundle\Entity\Agency $agencyiata
     *
     * @return Booking
     */
    public function setAgencyiata(\Travelport\GalileoBundle\Entity\Agency $agencyiata = null)
    {
        $this->agencyiata = $agencyiata;

        return $this;
    }

    /**
     * Get agencyiata
     *
     * @return \Travelport\GalileoBundle\Entity\Agency
     */
    public function getAgencyiata()
    {
        return $this->agencyiata;
    }

    /**
     * Set company
     *
     * @param \Travelport\GalileoBundle\Entity\Company $company
     *
     * @return Booking
     */
    public function setCompany(\Travelport\GalileoBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \Travelport\GalileoBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set agent
     *
     * @param \Travelport\GalileoBundle\Entity\Contact $agent
     *
     * @return Booking
     */
    public function setAgent(\Travelport\GalileoBundle\Entity\Contact $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \Travelport\GalileoBundle\Entity\Contact
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set agentiata
     *
     * @param \Travelport\GalileoBundle\Entity\Contact $agentiata
     *
     * @return Booking
     */
    public function setAgentiata(\Travelport\GalileoBundle\Entity\Contact $agentiata = null)
    {
        $this->agentiata = $agentiata;

        return $this;
    }

    /**
     * Get agentiata
     *
     * @return \Travelport\GalileoBundle\Entity\Contact
     */
    public function getAgentiata()
    {
        return $this->agentiata;
    }
}
