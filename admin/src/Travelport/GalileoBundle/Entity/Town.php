<?php

namespace Travelport\GalileoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;


/**
 * Town
 *
 * @ORM\Table(name="town")
 * @ORM\Entity(repositoryClass="Travelport\GalileoBundle\Repository\TownRepository")
 * @Serializer\ExclusionPolicy("ALL")
 */
class Town
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\OneToMany(targetEntity="Agency", cascade="persist", mappedBy="town")
    */
    private $agencies;

     /**
     * @ORM\OneToMany(targetEntity="Team", cascade="persist", mappedBy="town")
    */
    private $teams;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, unique=true)
     * @Serializer\Expose
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Serializer\Expose
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="datetimetz")
     */
    private $createdDate;

    /**
     * @var string
     *
     * @ORM\Column(name="createdBy", type="string", length=255)
     */
    private $createdBy;


    public function __construct(){
        $this->createdDate  = new \Datetime();
        $this->createdBy = "Simo Tresor";
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Town
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Town
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return Town
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    
        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     *
     * @return Town
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Add agency
     *
     * @param \Travelport\GalileoBundle\Entity\Agency $agency
     *
     * @return Town
     */
    public function addAgency(\Travelport\GalileoBundle\Entity\Agency $agency)
    {
        $this->agencies[] = $agency;
    
        return $this;
    }

    /**
     * Remove agency
     *
     * @param \Travelport\GalileoBundle\Entity\Agency $agency
     */
    public function removeAgency(\Travelport\GalileoBundle\Entity\Agency $agency)
    {
        $this->agencies->removeElement($agency);
    }

    /**
     * Get agencies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAgencies()
    {
        return $this->agencies;
    }

    /**
     * Add team
     *
     * @param \Travelport\GalileoBundle\Entity\Team $team
     *
     * @return Town
     */
    public function addTeam(\Travelport\GalileoBundle\Entity\Team $team)
    {
        $this->teams[] = $team;
    
        return $this;
    }

    /**
     * Remove team
     *
     * @param \Travelport\GalileoBundle\Entity\Team $team
     */
    public function removeTeam(\Travelport\GalileoBundle\Entity\Team $team)
    {
        $this->teams->removeElement($team);
    }

    /**
     * Get teams
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeams()
    {
        return $this->teams;
    }
}
