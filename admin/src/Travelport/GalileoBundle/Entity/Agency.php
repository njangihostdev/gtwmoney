<?php

namespace Travelport\GalileoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Agency
 *
 * @ORM\Table(name="agency")
 * @ORM\Entity(repositoryClass="Travelport\GalileoBundle\Repository\AgencyRepository")
 * 
 * @Serializer\ExclusionPolicy("ALL")
 */
class Agency
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose
     */
    private $id;



    /**
     * @ORM\ManyToOne(targetEntity="Town",  inversedBy="agencies")
     * @Serializer\Expose
    */
    private $town;


    /**
     * @ORM\OneToMany(targetEntity="Result", mappedBy="agency", cascade="persist" )
    */
    private $results;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Serializer\Expose
     */
    private $name;

     /**
     * @var boolean
     *
     * @ORM\Column(name="iata", type="boolean")
     * @Serializer\Expose
     */
    private $iata;


    /**
     * @var boolean
     *
     * @ORM\Column(name="work", type="boolean")
     * @Serializer\Expose
     */
    private $work;


    /**
     * @var string
     *
     * @ORM\Column(name="createdBy", type="string", length=255)
     */
    private $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="codeama", type="string", length=255)
     * @Serializer\Expose
     */
    private $codeama;

    /**
     * @var string
     *
     * @ORM\Column(name="codegal", type="string", length=255)
     * @Serializer\Expose
     */
    private $codegal;

    /**
     * @var string
     *
     * @ORM\Column(name="codesab", type="string", length=255)
     * @Serializer\Expose
     */
    private $codesab;

    /**
     * @var string
     *
     * @ORM\Column(name="locality", type="string", length=255)
     * @Serializer\Expose
     */
    private $locality;

     /**
     * @var int
     *
     * @ORM\Column(name="objectif", type="integer")
     */
    private $objectif;

    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="agencies")
    */
    private $header;

    /**
     * @ORM\OneToMany(targetEntity="Equipment", cascade="persist", mappedBy="agency")
    */
    private $equipments;

     /**
     * @ORM\OneToMany(targetEntity="Ticketing", cascade="persist", mappedBy="agency")
    */
    private $ticketings;

    /**
     * @ORM\OneToMany(targetEntity="Contact", cascade="persist", mappedBy="agency")
    */
    private $contacts;


    /**
     * @ORM\OneToMany(targetEntity="Booking", cascade="persist", mappedBy="agency")
    */
    private $bookings;


    /**
     * @ORM\OneToMany(targetEntity="Booking", cascade="persist", mappedBy="agencyiata")
    */
    private $bookingsiata;





    public function __construct(){
        $this->createdBy = "GTW Cameroon";
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Agency
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set createdBy
     *
     * @param string $createdBy
     *
     * @return Agency
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    

    /**
     * Set town
     *
     * @param \Travelport\GalileoBundle\Entity\Town $town
     *
     * @return Agency
     */
    public function setTown(\Travelport\GalileoBundle\Entity\Town $town = null)
    {
        $this->town = $town;
    
        return $this;
    }

    /**
     * Get town
     *
     * @return \Travelport\GalileoBundle\Entity\Town
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Add result
     *
     * @param \Travelport\GalileoBundle\Entity\Result $result
     *
     * @return Agency
     */
    public function addResult(\Travelport\GalileoBundle\Entity\Result $result)
    {
        $this->results[] = $result;
    
        return $this;
    }

    /**
     * Remove result
     *
     * @param \Travelport\GalileoBundle\Entity\Result $result
     */
    public function removeResult(\Travelport\GalileoBundle\Entity\Result $result)
    {
        $this->results->removeElement($result);
    }

    /**
     * Get results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * Set codeama
     *
     * @param string $codeama
     *
     * @return Agency
     */
    public function setCodeama($codeama)
    {
        $this->codeama = $codeama;
    
        return $this;
    }

    /**
     * Get codeama
     *
     * @return string
     */
    public function getCodeama()
    {
        return $this->codeama;
    }

    /**
     * Set codegal
     *
     * @param string $codegal
     *
     * @return Agency
     */
    public function setCodegal($codegal)
    {
        $this->codegal = $codegal;
    
        return $this;
    }

    /**
     * Get codegal
     *
     * @return string
     */
    public function getCodegal()
    {
        return $this->codegal;
    }

    /**
     * Set codesab
     *
     * @param string $codesab
     *
     * @return Agency
     */
    public function setCodesab($codesab)
    {
        $this->codesab = $codesab;
    
        return $this;
    }

    /**
     * Get codesab
     *
     * @return string
     */
    public function getCodesab()
    {
        return $this->codesab;
    }

    /**
     * Add equipment
     *
     * @param \Travelport\GalileoBundle\Entity\Equipment $equipment
     *
     * @return Agency
     */
    public function addEquipment(\Travelport\GalileoBundle\Entity\Equipment $equipment)
    {
        $this->equipments[] = $equipment;
    
        return $this;
    }

    /**
     * Remove equipment
     *
     * @param \Travelport\GalileoBundle\Entity\Equipment $equipment
     */
    public function removeEquipment(\Travelport\GalileoBundle\Entity\Equipment $equipment)
    {
        $this->equipments->removeElement($equipment);
    }

    /**
     * Get equipments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEquipments()
    {
        return $this->equipments;
    }

    /**
     * Set locality
     *
     * @param string $locality
     *
     * @return Agency
     */
    public function setLocality($locality)
    {
        $this->locality = $locality;
    
        return $this;
    }

    /**
     * Get locality
     *
     * @return string
     */
    public function getLocality()
    {
        return $this->locality;
    }


    /**
     * Add contact
     *
     * @param \Travelport\GalileoBundle\Entity\Contact $contact
     *
     * @return Agency
     */
    public function addContact(\Travelport\GalileoBundle\Entity\Contact $contact)
    {
        $this->contacts[] = $contact;
    
        return $this;
    }

    /**
     * Remove contact
     *
     * @param \Travelport\GalileoBundle\Entity\Contact $contact
     */
    public function removeContact(\Travelport\GalileoBundle\Entity\Contact $contact)
    {
        $this->contacts->removeElement($contact);
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Add ticketing
     *
     * @param \Travelport\GalileoBundle\Entity\Ticketing $ticketing
     *
     * @return Agency
     */
    public function addTicketing(\Travelport\GalileoBundle\Entity\Ticketing $ticketing)
    {
        $this->ticketings[] = $ticketing;
    
        return $this;
    }

    /**
     * Remove ticketing
     *
     * @param \Travelport\GalileoBundle\Entity\Ticketing $ticketing
     */
    public function removeTicketing(\Travelport\GalileoBundle\Entity\Ticketing $ticketing)
    {
        $this->ticketings->removeElement($ticketing);
    }

    /**
     * Get ticketings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTicketings()
    {
        return $this->ticketings;
    }

    /**
     * Set header
     *
     * @param \Travelport\GalileoBundle\Entity\Team $header
     *
     * @return Agency
     */
    public function setHeader(\Travelport\GalileoBundle\Entity\Team $header = null)
    {
        $this->header = $header;

        return $this;
    }

    /**
     * Get header
     *
     * @return \Travelport\GalileoBundle\Entity\Team
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Set iata
     *
     * @param boolean $iata
     *
     * @return Agency
     */
    public function setIata($iata)
    {
        $this->iata = $iata;

        return $this;
    }

    /**
     * Get iata
     *
     * @return boolean
     */
    public function getIata()
    {
        return $this->iata;
    }

    /**
     * Set work
     *
     * @param boolean $work
     *
     * @return Agency
     */
    public function setWork($work)
    {
        $this->work = $work;

        return $this;
    }

    /**
     * Get work
     *
     * @return boolean
     */
    public function getWork()
    {
        return $this->work;
    }

    /**
     * Set objectif
     *
     * @param integer $objectif
     *
     * @return Agency
     */
    public function setObjectif($objectif)
    {
        $this->objectif = $objectif;

        return $this;
    }

    /**
     * Get objectif
     *
     * @return integer
     */
    public function getObjectif()
    {
        return $this->objectif;
    }

    /**
     * Add booking
     *
     * @param \Travelport\GalileoBundle\Entity\Booking $booking
     *
     * @return Agency
     */
    public function addBooking(\Travelport\GalileoBundle\Entity\Booking $booking)
    {
        $this->bookings[] = $booking;

        return $this;
    }

    /**
     * Remove booking
     *
     * @param \Travelport\GalileoBundle\Entity\Booking $booking
     */
    public function removeBooking(\Travelport\GalileoBundle\Entity\Booking $booking)
    {
        $this->bookings->removeElement($booking);
    }

    /**
     * Get bookings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookings()
    {
        return $this->bookings;
    }

    /**
     * Add bookingsiatum
     *
     * @param \Travelport\GalileoBundle\Entity\Booking $bookingsiatum
     *
     * @return Agency
     */
    public function addBookingsiatum(\Travelport\GalileoBundle\Entity\Booking $bookingsiatum)
    {
        $this->bookingsiata[] = $bookingsiatum;

        return $this;
    }

    /**
     * Remove bookingsiatum
     *
     * @param \Travelport\GalileoBundle\Entity\Booking $bookingsiatum
     */
    public function removeBookingsiatum(\Travelport\GalileoBundle\Entity\Booking $bookingsiatum)
    {
        $this->bookingsiata->removeElement($bookingsiatum);
    }

    /**
     * Get bookingsiata
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookingsiata()
    {
        return $this->bookingsiata;
    }
}
