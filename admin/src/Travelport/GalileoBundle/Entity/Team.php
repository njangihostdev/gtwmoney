<?php

namespace Travelport\GalileoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;


/**
 * Team
 *
 * @ORM\Table(name="team")
 * @ORM\Entity(repositoryClass="Travelport\GalileoBundle\Repository\TeamRepository")
 * @Serializer\ExclusionPolicy("ALL")
 */
class Team
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Serializer\Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=255)
     * @Serializer\Expose
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Serializer\Expose
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=255)
     * @Serializer\Expose
     */
    private $role;

    /**
     * @var boolean
     *
     * @ORM\Column(name="disponibility", type="boolean")
     */
    private $disponibility;

    /**
     * @ORM\OneToMany(targetEntity="Ticketing", cascade="persist", mappedBy="technician")
    */
    private $ticketings;

    /**
     * @ORM\OneToMany(targetEntity="Ticketing", cascade="persist", mappedBy="helpdesk")
    */
    private $ticketingshelpdesk;

    /**
     * @ORM\OneToMany(targetEntity="moveEquipment", cascade="persist", mappedBy="technician")
    */
    private $moveEquipments;

    /**
     * @ORM\ManyToOne(targetEntity="Town",  inversedBy="teams")
     * @Serializer\Expose
    */
    private $town;

    /**
     * @ORM\OneToMany(targetEntity="Agency", mappedBy="header")
    */
    private $agencies;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Team
     */
    public function setNumber($number)
    {
        $this->number = $number;
    
        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Team
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return Team
     */
    public function setRole($role)
    {
        $this->role = $role;
    
        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ticketings = new \Doctrine\Common\Collections\ArrayCollection();

        $this->disponibility = true;

    }

    /**
     * Add ticketing
     *
     * @param \Travelport\GalileoBundle\Entity\Ticketing $ticketing
     *
     * @return Team
     */
    public function addTicketing(\Travelport\GalileoBundle\Entity\Ticketing $ticketing)
    {
        $this->ticketings[] = $ticketing;
    
        return $this;
    }

    /**
     * Remove ticketing
     *
     * @param \Travelport\GalileoBundle\Entity\Ticketing $ticketing
     */
    public function removeTicketing(\Travelport\GalileoBundle\Entity\Ticketing $ticketing)
    {
        $this->ticketings->removeElement($ticketing);
    }

    /**
     * Get ticketings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTicketings()
    {
        return $this->ticketings;
    }

    /**
     * Set disponibility
     *
     * @param boolean $disponibility
     *
     * @return Team
     */
    public function setDisponibility($disponibility)
    {
        $this->disponibility = $disponibility;
    
        return $this;
    }

    /**
     * Get disponibility
     *
     * @return boolean
     */
    public function getDisponibility()
    {
        return $this->disponibility;
    }

    /**
     * Add moveEquipment
     *
     * @param \Travelport\GalileoBundle\Entity\moveEquipment $moveEquipment
     *
     * @return Team
     */
    public function addMoveEquipment(\Travelport\GalileoBundle\Entity\moveEquipment $moveEquipment)
    {
        $this->moveEquipments[] = $moveEquipment;
    
        return $this;
    }

    /**
     * Remove moveEquipment
     *
     * @param \Travelport\GalileoBundle\Entity\moveEquipment $moveEquipment
     */
    public function removeMoveEquipment(\Travelport\GalileoBundle\Entity\moveEquipment $moveEquipment)
    {
        $this->moveEquipments->removeElement($moveEquipment);
    }

    /**
     * Get moveEquipments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMoveEquipments()
    {
        return $this->moveEquipments;
    }

    /**
     * Set town
     *
     * @param \Travelport\GalileoBundle\Entity\Town $town
     *
     * @return Team
     */
    public function setTown(\Travelport\GalileoBundle\Entity\Town $town = null)
    {
        $this->town = $town;
    
        return $this;
    }

    /**
     * Get town
     *
     * @return \Travelport\GalileoBundle\Entity\Town
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Add ticketingshelpdesk
     *
     * @param \Travelport\GalileoBundle\Entity\Ticketing $ticketingshelpdesk
     *
     * @return Team
     */
    public function addTicketingshelpdesk(\Travelport\GalileoBundle\Entity\Ticketing $ticketingshelpdesk)
    {
        $this->ticketingshelpdesk[] = $ticketingshelpdesk;
    
        return $this;
    }

    /**
     * Remove ticketingshelpdesk
     *
     * @param \Travelport\GalileoBundle\Entity\Ticketing $ticketingshelpdesk
     */
    public function removeTicketingshelpdesk(\Travelport\GalileoBundle\Entity\Ticketing $ticketingshelpdesk)
    {
        $this->ticketingshelpdesk->removeElement($ticketingshelpdesk);
    }

    /**
     * Get ticketingshelpdesk
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTicketingshelpdesk()
    {
        return $this->ticketingshelpdesk;
    }

    /**
     * Add agency
     *
     * @param \Travelport\GalileoBundle\Entity\Agency $agency
     *
     * @return Team
     */
    public function addAgency(\Travelport\GalileoBundle\Entity\Agency $agency)
    {
        $this->agencies[] = $agency;

        return $this;
    }

    /**
     * Remove agency
     *
     * @param \Travelport\GalileoBundle\Entity\Agency $agency
     */
    public function removeAgency(\Travelport\GalileoBundle\Entity\Agency $agency)
    {
        $this->agencies->removeElement($agency);
    }

    /**
     * Get agencies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAgencies()
    {
        return $this->agencies;
    }
}
