<?php

namespace Travelport\GalileoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Travelport\GalileoBundle\Service\ResultService;
use Travelport\GalileoBundle\Entity\Result;
use Travelport\GalileoBundle\Entity\Operator;
use Travelport\GalileoBundle\Entity\Agency;
use Travelport\GalileoBundle\Entity\Company;
use Travelport\GalileoBundle\Entity\Town;
use Travelport\GalileoBundle\Service\OperatorService;
use Travelport\GalileoBundle\Service\AgencyService;
use Travelport\GalileoBundle\Service\CompanyService;
use Travelport\GalileoBundle\Service\TownService;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Doctrine\ORM\QueryBuilder;
use CoreBundle\Exception\RessourceValidationException;
use FOS\RestBundle\Controller\FOSRestController;
use Travelport\GalileoBundle\Classes\GalileoResultAgency;
use Travelport\GalileoBundle\Classes\GalileoResultTown;
use Travelport\GalileoBundle\Classes\GalileoResultCompany;



class AirController extends Controller
{


     /**
    *@Rest\Get("/api/galileo/rest/agency")
     *@Rest\QueryParam(
    *   name="xml"
    *)
    *@Rest\View()
    */
    public function executeAction(ParamFetcherInterface $paramFetcher)
    {
        $message = $paramFetcher->get('xml');

        $TARGETBRANCH = 'P105322';
        $CREDENTIALS = 'Universal API/uAPI-774137105:by2D4MT2c45M6G4WtPb7NtWwM';
        $Provider = '1G';//1G/1V/1P/ACH
        $file = "001-".$Provider."_LowFareSearchReq.xml"; // file name to save the request xml for test only(if you want to save the request/response)
        $this->prettyPrint($message,$file);//call function to pretty print xml
        $auth = base64_encode("$CREDENTIALS");
        $soap_do = curl_init("https://emea.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/UniversalRecordService");
        /*("https://americas.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/AirService");*/
        $header = array(
        "Content-Type: text/xml;charset=UTF-8",
        "Accept-Encoding: gzip,deflate",
        "Cache-Control: no-cache",
        "Pragma: no-cache",
        "SOAPAction: \"\"",
        "Authorization: Basic $auth",
        "Content-length: ".strlen($message),
        );
        //curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 30);
        //curl_setopt($soap_do, CURLOPT_TIMEOUT, 30);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST, true );
        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $message);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, $header);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($soap_do, CURLOPT_ENCODING, '');
        $return = curl_exec($soap_do);
        var_dump($return);
        curl_close($soap_do);
        $file = "001-".$Provider."_LowFareSearchRsp.xml";
        $content = $this->prettyPrint($return,$file);
       // var_dump($content);
        $xml = simplexml_load_string($content);
        $posts = $xml->children('SOAP', true)->Body->children('air', true);
        $result = $this->xmlObjToArr($posts);
        $data = json_encode($result);
        $response = new Response($data);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

	 /**
    *@Rest\Get("/api/galileo/rest/agency")
     *@Rest\QueryParam(
    *   name="depart"
    *)
    *@Rest\QueryParam(
    *   name="arrive"
    *)
    *@Rest\QueryParam(
    *   name="origin"
    *)
    *@Rest\QueryParam(
    *   name="destination"
    *)
     *@Rest\QueryParam(
    *   name="carrier"
    *)
      *@Rest\QueryParam(
    *   name="cabin"
    *)
    *@Rest\View()
    */
    public function lowsearchAction(ParamFetcherInterface $paramFetcher)
    {
        $depart = $paramFetcher->get('depart');
        $arrive = $paramFetcher->get('arrive');
        $origin = $paramFetcher->get('origin');
        $destination = $paramFetcher->get('destination');
        $carrier = $paramFetcher->get('carrier');
        $cabin = $paramFetcher->get('cabin');


        $TARGETBRANCH = 'P105221';
        $CREDENTIALS = 'Universal API/uAPI2656018225:fhQYZNMNTMBMDEpRecy56zTxD';
        $Provider = '1G';//1G/1V/1P/ACH
        $PreferredDepDate = date($depart);//This should be taken from User input in HTML/PHP user interface
        $PreferredRetDate = date($arrive);//This should be taken from User input in HTML/PHP user interface
        $NumberOfTravelers = 1;
        $Origin = $origin; //This should be taken from User input in HTML/PHP user interface
        $Destination = $destination; //This should be taken from User input in HTML/PHP user interface
        $Carrier = $carrier;//This should be taken from User input in HTML/PHP user interface
        $CabinClass = $cabin;//This should be taken from User input in HTML/PHP user interface
        //Create SOAP XML
        $message = new \DOMDocument('1.0', 'UTF-8');
        //$xmlRoot = $message->createElement("xml");
        //$xmlRoot = $message->appendChild($xmlRoot);
        //Create Envelope
        $xmlRoot = $message->createElementNS("http://schemas.xmlsoap.org/soap/envelope/","soapenv:Envelope","");
        /*$xmlRootAttribute = $message->createAttribute('xmlns:soapenv');
        // Value for the created attribute
        $xmlRootAttribute->value = 'http://schemas.xmlsoap.org/soap/envelope/';
        $xmlRoot->appendChild($xmlRootAttribute);*/
        $xmlRoot = $message->appendChild($xmlRoot);
        //Create Header
        $xmlRootHeader = $message->createElement("soapenv:Header");
        $xmlRootHeader = $xmlRoot->appendChild($xmlRootHeader);
        //Create Body
        $xmlRootBody = $message->createElement("soapenv:Body");
        $xmlRootBody = $xmlRoot->appendChild($xmlRootBody);
        $lfsRootNode = $message->createElementNS("http://www.travelport.com/schema/air_v42_0","air:LowFareSearchReq","");
        $lfsRootNode = $xmlRootBody->appendChild($lfsRootNode);
        $lfsRootNodeattribute = $message->createAttribute("TraceId");
        $lfsRootNodeattribute->value = "trace";
        $lfsRootNode->appendChild($lfsRootNodeattribute);
        $lfsRootNodeattribute = $message->createAttribute("AuthorizedBy");
        $lfsRootNodeattribute->value = "user";
        $lfsRootNode->appendChild($lfsRootNodeattribute);
        $lfsRootNodeattribute = $message->createAttribute("TargetBranch");
        $lfsRootNodeattribute->value = $TARGETBRANCH;
        $lfsRootNode->appendChild($lfsRootNodeattribute);
        //Above part of code will be same for almost all the requests in UAPI
        //Below part of code might be different based on request structure
        $lfsRootNodeattribute = $message->createAttribute("SolutionResult");
        $lfsRootNodeattribute->value = "true";//If PricePoint is needed then we need to pass the value as false, bydefault this valus is false, incase of true we will receive PricingSolutions
        $lfsRootNode->appendChild($lfsRootNodeattribute);
        $billPointOfSaleNode = $message->createElementNS("http://www.travelport.com/schema/common_v42_0","com:BillingPointOfSaleInfo","");
        $billPointOfSaleNodeattribute = $message->createAttribute("OriginApplication");
        $billPointOfSaleNodeattribute->value = "UAPI";
        $billPointOfSaleNode->appendChild($billPointOfSaleNodeattribute);
        $billPointOfSaleNode = $lfsRootNode->appendChild($billPointOfSaleNode);
        //Outbound Flight Request
        $outboundFlightLeg = $message->createElement("air:SearchAirLeg");
        $outboundFlightLeg = $lfsRootNode->appendChild($outboundFlightLeg);
        $originLeg = $message->createElement("air:SearchOrigin");
        $originLeg = $outboundFlightLeg->appendChild($originLeg);
        $destinatonLeg = $message->createElement("air:SearchDestination");
        $destinatonLeg = $outboundFlightLeg->appendChild($destinatonLeg);
        $prefOutDate = $message->createElement("air:SearchDepTime");
        $prefOutDateAttribute = $message->createAttribute("PreferredTime");
        $prefOutDateAttribute->value = $PreferredDepDate;
        $prefOutDate->appendChild($prefOutDateAttribute);
        $prefOutDate = $outboundFlightLeg->appendChild($prefOutDate);
        //It can be aiportCode, or City Code, or CityOrAirportCode with any one of them as preferrence
        $aiportCode = $message->createElementNS("http://www.travelport.com/schema/common_v42_0","com:Airport","");
        $aiportCodeattribute = $message->createAttribute("Code");
        $aiportCodeattribute->value = $Origin;
        $aiportCode->appendChild($aiportCodeattribute);
        $aiportCode = $originLeg->appendChild($aiportCode);
        $aiportCode = $message->createElementNS("http://www.travelport.com/schema/common_v42_0","com:Airport","");
        $aiportCodeattribute = $message->createAttribute("Code");
        $aiportCodeattribute->value = $Destination;
        $aiportCode->appendChild($aiportCodeattribute);
        $aiportCode = $destinatonLeg->appendChild($aiportCode);

        if($arrive != null){
             //Inbound/Return Flight Request
        //we can add this part as a inside a if block if return flight requested by the user
        $inboundFlightLeg = $message->createElement("air:SearchAirLeg");
        $inboundFlightLeg = $lfsRootNode->appendChild($inboundFlightLeg);
        $originLeg = $message->createElement("air:SearchOrigin");
        $originLeg = $inboundFlightLeg->appendChild($originLeg);
        $destinatonLeg = $message->createElement("air:SearchDestination");
        $destinatonLeg = $inboundFlightLeg->appendChild($destinatonLeg);
        $prefOutDate = $message->createElement("air:SearchDepTime");
        $prefOutDateAttribute = $message->createAttribute("PreferredTime");
        $prefOutDateAttribute->value = $PreferredRetDate;
        $prefOutDate->appendChild($prefOutDateAttribute);
        $prefOutDate = $inboundFlightLeg->appendChild($prefOutDate);
        //It can be aiportCode, or City Code, or CityOrAirportCode with any one of them as preferrence
        $aiportCode = $message->createElementNS("http://www.travelport.com/schema/common_v42_0","com:Airport","");
        $aiportCodeattribute = $message->createAttribute("Code");
        $aiportCodeattribute->value = $Destination;
        $aiportCode->appendChild($aiportCodeattribute);
        $aiportCode = $originLeg->appendChild($aiportCode);
        $aiportCode = $message->createElementNS("http://www.travelport.com/schema/common_v42_0","com:Airport","");
        $aiportCodeattribute = $message->createAttribute("Code");
        $aiportCodeattribute->value = $Origin;
        $aiportCode->appendChild($aiportCodeattribute);
        $aiportCode = $destinatonLeg->appendChild($aiportCode);
        //Below part is modifiers and optional data
        }
       

        if($carrier != null){
             $airSearchModifiersNode = $message->createElement("air:AirSearchModifiers");
        $airSearchModifiersNode = $lfsRootNode->appendChild($airSearchModifiersNode);
        $prefProviderNode = $message->createElement("air:PreferredProviders");
        $prefProviderNode = $airSearchModifiersNode->appendChild($prefProviderNode);
        $prefCabinNode = $message->createElement("air:PermittedCabins");
        $prefCabinNode = $airSearchModifiersNode->appendChild($prefCabinNode);
        $prefCarrierNode = $message->createElement("air:PermittedCarriers");
        $prefCarrierNode = $airSearchModifiersNode->insertBefore($prefCarrierNode, $prefCabinNode);//this has been added just to show that a particular node can be inserted before a particualr node dynamically
        //if there is multiple provider as permitted or preferred we can run this code in a loop to add all the carriers permitted
        $perfProviderCodeNode = $message->createElementNS("http://www.travelport.com/schema/common_v42_0","com:Provider","");
        $perfProviderCodeNodeattribute = $message->createAttribute("Code");
        $perfProviderCodeNodeattribute->value = $Provider;
        $perfProviderCodeNode->appendChild($perfProviderCodeNodeattribute);
        $perfProviderCodeNode = $prefProviderNode->appendChild($perfProviderCodeNode);
        //if there is multiple carrier as permitted or preferred we can run this code in a loop to add all the carriers permitted
        $perfCarrierCodeNode = $message->createElementNS("http://www.travelport.com/schema/common_v42_0","com:Carrier","");
        $perfCarrierCodeNodeattribute = $message->createAttribute("Code");
        $perfCarrierCodeNodeattribute->value = $Carrier;
        $perfCarrierCodeNode->appendChild($perfCarrierCodeNodeattribute);
        $perfCarrierCodeNode = $prefCarrierNode->appendChild($perfCarrierCodeNode);
    
        
        $perfCabinCodeNode = $message->createElementNS("http://www.travelport.com/schema/common_v42_0","com:CabinClass","");
        $perfCabinCodeNodeattribute = $message->createAttribute("Type");
        $perfCabinCodeNodeattribute->value = $CabinClass;
        $perfCabinCodeNode->appendChild($perfCabinCodeNodeattribute);
        $perfCabinCodeNode = $prefCabinNode->appendChild($perfCabinCodeNode);
        }
       
        //Below part is to add number of traveler in the request
        for($i = 0; $i < $NumberOfTravelers; $i++)
        {
            $travelerDetails = $message->createElementNS("http://www.travelport.com/schema/common_v42_0","com:SearchPassenger","");
            $travelerDetailsattribute = $message->createAttribute("BookingTravelerRef");
            $travelerDetailsattribute->value = $i;
            $travelerDetails->appendChild($travelerDetailsattribute);
            $travelerDetailsattribute = $message->createAttribute("Code");
            $travelerDetailsattribute->value = "ADT";
            $travelerDetails->appendChild($travelerDetailsattribute);
            $travelerDetails = $lfsRootNode->appendChild($travelerDetails);
        }
        $message->preserveWhiteSpace = false;
        $message->formatOutput = true;
        $message = $message->saveXML();
        $file = "001-".$Provider."_LowFareSearchReq.xml"; // file name to save the request xml for test only(if you want to save the request/response)
        $this->prettyPrint($message,$file);//call function to pretty print xml
        $auth = base64_encode("$CREDENTIALS");
        $soap_do = curl_init("https://apac.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/AirService");
        /*("https://americas.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/AirService");*/
        $header = array(
        "Content-Type: text/xml;charset=UTF-8",
        "Accept-Encoding: gzip,deflate",
        "Cache-Control: no-cache",
        "Pragma: no-cache",
        "SOAPAction: \"\"",
        "Authorization: Basic $auth",
        "Content-length: ".strlen($message),
        );
        //curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 30);
        //curl_setopt($soap_do, CURLOPT_TIMEOUT, 30);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST, true );
        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $message);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, $header);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($soap_do, CURLOPT_ENCODING, '');
        $return = curl_exec($soap_do);
        curl_close($soap_do);
        $file = "001-".$Provider."_LowFareSearchRsp.xml"; // file name to save the response xml for test only(if you want to save the request/response)
        $content = $this->prettyPrint($return,$file);
        //$this->parseOutput($content);
        // Loads the XML
        $xml = simplexml_load_string($content);

        // Grabs the posts
        $posts = $xml->children('SOAP', true)->Body->children('air', true);

        $result = $this->xmlObjToArr($posts);

        $data = json_encode($result);

        $response = new Response($data);
         //$response‐>headers‐>set('Content‐Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Content-Type', 'application/json');
        return $response;
        
    //outputWriter($file, $return);
    //print_r(curl_getinfo($soap_do));

    }


function xmlObjToArr($obj) { 
        $namespace = $obj->getDocNamespaces(true); 
        $namespace[NULL] = NULL; 
        
        $children = array(); 
        $attributes = array(); 
        $name = strtolower((string)$obj->getName()); 
        
        $text = trim((string)$obj); 
        if( strlen($text) <= 0 ) { 
            $text = NULL; 
        } 
        
        // get info for all namespaces 
        if(is_object($obj)) { 
            foreach( $namespace as $ns=>$nsUrl ) { 
                // atributes 
                $objAttributes = $obj->attributes($ns, true); 
                foreach( $objAttributes as $attributeName => $attributeValue ) { 
                    $attribName = strtolower(trim((string)$attributeName)); 
                    $attribVal = trim((string)$attributeValue); 
                    if (!empty($ns)) { 
                        $attribName =  $attribName; 
                    } 
                    $attributes[$attribName] = $attribVal; 
                } 
                
                // children 
                $objChildren = $obj->children($ns, true); 
                foreach( $objChildren as $childName=>$child ) { 
                    $childName = strtolower((string)$childName); 
                    if( !empty($ns) ) { 
                        $childName = $childName; 
                    } 
                    $children[$childName][] = $this->xmlObjToArr($child); 
                } 
            } 
        } 
        
        return array( 
            'name'=>$name, 
            'text'=>$text, 
            'attributes'=>$attributes, 
            'children'=>$children 
        ); 
    } 


    //Pretty print XML
function prettyPrint($result,$file){
    $dom = new \DOMDocument;
    $dom->preserveWhiteSpace = false;
    $dom->loadXML($result);
    $dom->formatOutput = true;
    //call function to write request/response in file
    $this->outputWriter($file,$dom->saveXML());
    return $dom->saveXML();
}
//function to write output in a file
function outputWriter($file,$content){
    file_put_contents($file, $content); // Write request/response and save them in the File
}
function ListAirSegments($key, $lowFare){
    foreach($lowFare->children('air',true) as $airSegmentList){
        if(strcmp($airSegmentList->getName(),'AirSegmentList') == 0){
            foreach($airSegmentList->children('air', true) as $airSegment){
                if(strcmp($airSegment->getName(),'AirSegment') == 0){
                    foreach($airSegment->attributes() as $a => $b){
                        if(strcmp($a,'Key') == 0){
                            if(strcmp($b, $key) == 0){
                                return $airSegment;
                            }
                        }
                    }
                }
            }
        }
    }
}
function PriceSegment($segment){
}
function parseOutput($content){ //parse the Search response to get values to use in detail request
    $LowFareSearchRsp = $content; //use this if response is not saved anywhere else use above variable
    //echo $LowFareSearchRsp;
    $xml = simplexml_load_String("$LowFareSearchRsp", null, null, 'SOAP', true);
    if($xml)
        echo "Processing! Please wait!";
    else{
        //trigger_error("Encoding Error!", E_USER_ERROR);
        $error = "Encoding Error! Check SOAP Fault";
        throw new Exception($error);
    }
    $Results = $xml->children('SOAP',true);
    foreach($Results->children('SOAP',true) as $fault){
        if(strcmp($fault->getName(),'Fault') == 0){
            //trigger_error("Error occurred request/response processing!", E_USER_ERROR);
            $error = "Error occurred request/response processing! Check SOAP Fault";
            throw new Exception($error);
        }
    }
    /*$count = $count + 1;
                        file_put_contents($fileName,"\r\n"."Air Segment ".$count."\r\n"."\r\n", FILE_APPEND);
                        foreach($hp->attributes() as $a => $b   ){
                                $GLOBALS[$a] = "$b";
                                //echo "$a"." : "."$b";
                                file_put_contents($fileName,$a." : ".$b."\r\n", FILE_APPEND);
                        }*/
    $count = 0;
    $fileName = "flights.txt";
    if(file_exists($fileName)){
        file_put_contents($fileName, "");
    }
    foreach($Results->children('air',true) as $lowFare){
        foreach($lowFare->children('air',true) as $airPriceSol){
            if(strcmp($airPriceSol->getName(),'AirPricingSolution') == 0){
                $count = $count + 1;
                file_put_contents($fileName, "Air Pricing Solutions Details ".$count.":\r\n", FILE_APPEND);
                file_put_contents($fileName,"--------------------------------------\r\n", FILE_APPEND);
                foreach($airPriceSol->children('air',true) as $journey){
                    if(strcmp($journey->getName(),'Journey') == 0){
                        file_put_contents($fileName,"\r\nJourney Details: ", FILE_APPEND);
                        file_put_contents($fileName,"\r\n", FILE_APPEND);
                        file_put_contents($fileName,"--------------------------------------\r\n", FILE_APPEND);
                        foreach($journey->children('air', true) as $segmentRef){
                            if(strcmp($segmentRef->getName(),'AirSegmentRef') == 0){
                                foreach($segmentRef->attributes() as $a => $b){
                                    $segment = $this->ListAirSegments($b, $lowFare);
                                    foreach($segment->attributes() as $c => $d){
                                        if(strcmp($c, "Origin") == 0){
                                            file_put_contents($fileName,"From ".$d."\r\n", FILE_APPEND);
                                        }
                                        if(strcmp($c, "Destination") == 0){
                                            file_put_contents($fileName,"To ".$d."\r\n", FILE_APPEND);
                                        }
                                        if(strcmp($c, "Carrier") == 0){
                                            file_put_contents($fileName,"Airline: ".$d."\r\n", FILE_APPEND);
                                        }
                                        if(strcmp($c, "FlightNumber") == 0){
                                            file_put_contents($fileName,"Flight ".$d."\r\n", FILE_APPEND);
                                        }
                                        if(strcmp($c, "DepartureTime") == 0){
                                            file_put_contents($fileName,"Depart ".$d."\r\n", FILE_APPEND);
                                        }
                                        if(strcmp($c, "ArrivalTime") == 0){
                                            file_put_contents($fileName,"Arrive ".$d."\r\n", FILE_APPEND);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                foreach($airPriceSol->children('air',true) as $priceInfo){
                    if(strcmp($priceInfo->getName(),'AirPricingInfo') == 0){
                        file_put_contents($fileName,"\r\nPricing Details: ", FILE_APPEND);
                        file_put_contents($fileName,"\r\n", FILE_APPEND);
                        file_put_contents($fileName,"--------------------------------------\r\n", FILE_APPEND);
                        foreach($priceInfo->attributes() as $e => $f){
                                if(strcmp($e, "ApproximateBasePrice") == 0){
                                    file_put_contents($fileName,"Approx. Base Price: ".$f."\r\n", FILE_APPEND);
                                }
                                if(strcmp($e, "ApproximateTaxes") == 0){
                                    file_put_contents($fileName,"Approx. Taxes: ".$f."\r\n", FILE_APPEND);
                                }
                                if(strcmp($e, "ApproximateTotalPrice") == 0){
                                    file_put_contents($fileName,"Approx. Total Price: ".$f."\r\n", FILE_APPEND);
                                }
                                if(strcmp($e, "BasePrice") == 0){
                                    file_put_contents($fileName,"Base Price: ".$f."\r\n", FILE_APPEND);
                                }
                                if(strcmp($e, "Taxes") == 0){
                                    file_put_contents($fileName,"Taxes: ".$f."\r\n", FILE_APPEND);
                                }
                                if(strcmp($e, "TotalPrice") == 0){
                                    file_put_contents($fileName,"Total Price: ".$f."\r\n", FILE_APPEND);
                                }
                        }
                    }
                }
                file_put_contents($fileName,"\r\n", FILE_APPEND);
                break;
            }
        }
    }
    $this->PriceSegment($segment);
    $Token = 'Token';
    $TokenKey = 'TokenKey';
    $fileName = "tokens.txt";
    if(file_exists($fileName)){
        file_put_contents($fileName, "");
    }
    foreach($Results->children('air',true) as $nodes){
        foreach($nodes->children('air',true) as $hsr){
            if(strcmp($hsr->getName(),'HostTokenList') == 0){
                foreach($hsr->children('common_v29_0', true) as $ht){
                    if(strcmp($ht->getName(), 'HostToken') == 0){
                        $GLOBALS[$Token] = $ht[0];
                        foreach($ht->attributes() as $a => $b){
                            if(strcmp($a, 'Key') == 0){
                                file_put_contents($fileName,$TokenKey.":".$b."\r\n", FILE_APPEND);
                            }
                        }
                        file_put_contents($fileName,$Token.":".$ht[0]."\r\n", FILE_APPEND);
                    }
                }
            }
        }
    }
    echo "\r\n"."Processing Done. Please check results in files.";
}

}
