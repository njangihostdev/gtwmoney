<?php

namespace Travelport\GalileoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Travelport\GalileoBundle\Service\ResultService;
use Travelport\GalileoBundle\Entity\Result;
use Travelport\GalileoBundle\Entity\Operator;
use Travelport\GalileoBundle\Entity\Agency;
use Travelport\GalileoBundle\Entity\Company;
use Travelport\GalileoBundle\Entity\Town;
use Travelport\GalileoBundle\Service\OperatorService;
use Travelport\GalileoBundle\Service\AgencyService;
use Travelport\GalileoBundle\Service\CompanyService;
use Travelport\GalileoBundle\Service\TownService;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Doctrine\ORM\QueryBuilder;
use CoreBundle\Exception\RessourceValidationException;
use FOS\RestBundle\Controller\FOSRestController;
use Travelport\GalileoBundle\Classes\GalileoResultAgency;
use Travelport\GalileoBundle\Classes\GalileoResultTown;
use Travelport\GalileoBundle\Classes\GalileoResultCompany;



class ResultController extends Controller
{

	private $resultService;
	private $operatorService;
	private $agencyService;
	private $companyService;
	private $townService;

     /**
    *@Rest\Get("/api/galileo/rest/agency")
    *@Rest\View()
    */
    public function api(ParamFetcherInterface $paramFetcher)
    {
         $this->agencyService = $this->get('travelport_galileo.service.agency');
         $results = $this->agencyService->rest();        
         $data = $this->get('jms_serializer')->serialize($results, 'json');
         $response = new Response($data);
         //$response‐>headers‐>set('Content‐Type', 'application/json');
         $response->headers->set('Content-Type', 'application/json');
         return $response;
    }


    public function indexAction()
    {
        return $this->render('TravelportGalileoBundle:Default:index.html.twig');
    }

    /**
    *@Rest\Get("/api/galileo/results/clean")
    *@Rest\QueryParam(
    *   name="date"
    *)
    *@Rest\View()
    */
    public function cleanAction(ParamFetcherInterface $paramFetcher)
    {
        $this->agencyService = $this->get('travelport_galileo.service.agency');
        $this->resultService = $this->get('travelport_galileo.service.result');
        $this->companyService = $this->get('travelport_galileo.service.company');
        $this->operatorService = $this->get('travelport_galileo.service.operator');

        $date = $paramFetcher->get('date');


        $agencies = $this->agencyService->listPerPage();
        $companies = $this->companyService->listPerPage();
        $operators = $this->operatorService->listPerPage();

        $results =  $this->resultService->getByDate($date);
        $resultat = array();

        foreach ($results as $r) {
            foreach ($results as $s) {
                if( $s->getId()!= $r->getId() and $s->getOperator()->getCode()== $r->getOperator()->getCode() and $s->getAgency()->getName() == $r->getAgency()->getName() and $s->getAgency()->getTown()->getCode() == $r->getAgency()->getTown()->getCode()  and $s->getCompany()->getCode()== $r->getCompany()->getCode() )
                {
                    array_push($resultat, $r);
                }
            }
        }

        /*foreach ($agencies as $agency) {
            foreach ($companies as $company) {
                foreach ($operators as $operator) {
                    $r = $this->resultService->same($operator->getCode(),$agency->getName(),$agency->getTown()->getCode(),$company->getCode(),$date);
                    if(count($r) > 1)
                    {
                       $galileo = new GalileoResultAgency();
                       $galileo->agency = $agency;
                         array_push($resultat, $galileo);
                    }
                }
            }
        }*/
        return $this->render('TravelportGalileoBundle:Default:clean.html.twig', array(
            'resultat' => $resultat
        ));

    }


     /**
    *@Rest\Get("/api/galileo/results/company")
    *@Rest\QueryParam(
    *   name="begin"
    *)
    *@Rest\QueryParam(
    *   name="end"
    *)
    *@Rest\View()
    */
    public function companyAction(ParamFetcherInterface $paramFetcher)
    {
        $begin = $paramFetcher->get('begin');
        $end = $paramFetcher->get('end');
        $this->companyService = $this->get('travelport_galileo.service.company');
        $this->resultService = $this->get('travelport_galileo.service.result');
        $companies = $this->companyService->listPerPage();
        $resultat = array();
        foreach ($companies as $company) {
            $results = $this->resultService->getByCompany($company,$begin,$end);
            $galileo = new GalileoResultCompany();
            $galileo->company = $company;
            foreach ($results as $result) {
                if($result->getOperator()->getCode() == "AMA")
                {
                    $galileo->ama = $galileo->ama + $result->getSegment();
                }
                if($result->getOperator()->getCode() == "GAL")
                {
                    $galileo->gal = $galileo->gal + $result->getSegment();
                }
                if($result->getOperator()->getCode() == "SAB")
                {
                    $galileo->sab = $galileo->sab + $result->getSegment();
                }

            }

            array_push($resultat, $galileo);
        }
        return $this->render('TravelportGalileoBundle:Default:company.html.twig', array(
            'resultat' => $resultat,
            'begin' => $begin,
            'end' => $end
        ));

    }


      /**
    *@Rest\Get("/api/galileo/results/cancel")
    *@Rest\QueryParam(
    *   name="begin"
    *)
    *@Rest\QueryParam(
    *   name="end"
    *)
    *@Rest\View()
    */
    public function cancelAction(ParamFetcherInterface $paramFetcher)
    {
        $begin = $paramFetcher->get('begin');
        $end = $paramFetcher->get('end');
        $this->companyService = $this->get('travelport_galileo.service.company');
        $this->resultService = $this->get('travelport_galileo.service.result');
        $companies = $this->companyService->listPerPage();
        $resultat = array();
        foreach ($companies as $company) {
            $results = $this->resultService->getByCompany($company,$begin,$end);
            $galileo = new GalileoResultCompany();
            $galileo->company = $company;
            foreach ($results as $result) {
                if($result->getSegment() < 0){
                    if($result->getOperator()->getCode() == "AMA")
                    {
                        $galileo->ama = $galileo->ama + $result->getSegment();
                    }
                    if($result->getOperator()->getCode() == "GAL")
                    {
                        $galileo->gal = $galileo->gal + $result->getSegment();
                    }
                    if($result->getOperator()->getCode() == "SAB")
                    {
                        $galileo->sab = $galileo->sab + $result->getSegment();
                    }
                }
                
            }

            array_push($resultat, $galileo);
        }
        return $this->render('TravelportGalileoBundle:Default:cancel.html.twig', array(
            'resultat' => $resultat,
            'begin' => $begin,
            'end' => $end
        ));

    }


    /**
    *@Rest\Get("/api/galileo/results/agency")
    *@Rest\QueryParam(
    *   name="begin"
    *)
    *@Rest\QueryParam(
    *   name="end"
    *)
    *@Rest\View()
    */
    public function agencyAction(ParamFetcherInterface $paramFetcher)
    {
        $begin = $paramFetcher->get('begin');
        $end = $paramFetcher->get('end');
        $this->agencyService = $this->get('travelport_galileo.service.agency');
        $this->resultService = $this->get('travelport_galileo.service.result');
        $agencies = $this->agencyService->listPerPage();
        $resultat = array();
        foreach ($agencies as $agency) {
            $results = $this->resultService->getByAgency($agency,$begin,$end);
            $galileo = new GalileoResultAgency();
            $galileo->agency = $agency;
            foreach ($results as $result) {
                if($result->getOperator()->getCode() == "AMA")
                {
                    $galileo->ama = $galileo->ama + $result->getSegment();
                }
                if($result->getOperator()->getCode() == "GAL")
                {
                    $galileo->gal = $galileo->gal + $result->getSegment();
                }
                if($result->getOperator()->getCode() == "SAB")
                {
                    $galileo->sab = $galileo->sab + $result->getSegment();
                }

            }

            array_push($resultat, $galileo);
        }

        return $this->render('TravelportGalileoBundle:Default:result.html.twig', array(
            'resultat' => $resultat,
            'begin' => $begin,
            'end' => $end,
        ));




    }


    
    /**
    *@Rest\Get("/api/galileo/results/report")
    *@Rest\QueryParam(
    *   name="begin"
    *)
    *@Rest\QueryParam(
    *   name="end"
    *)
    *@Rest\QueryParam(
    *   name="pcc"
    *)
    *@Rest\View()
    */
    public function reportAction(ParamFetcherInterface $paramFetcher)
    {
        $begin = $paramFetcher->get('begin');
        $end = $paramFetcher->get('end');
        $pcc = $paramFetcher->get('pcc');
        $this->agencyService = $this->get('travelport_galileo.service.agency');
        $this->resultService = $this->get('travelport_galileo.service.result');
        $agency = $this->agencyService->findByCodegal($pcc);
        $resultat = array();
            $results = $this->resultService->getByAgency($agency,$begin,$end);
            $galileo = new GalileoResultAgency();
            $galileo->agency = $agency;
            foreach ($results as $result) {
                if($result->getOperator()->getCode() == "AMA")
                {
                    $galileo->ama = $galileo->ama + $result->getSegment();
                }
                if($result->getOperator()->getCode() == "GAL")
                {
                    $galileo->gal = $galileo->gal + $result->getSegment();
                }
                if($result->getOperator()->getCode() == "SAB")
                {
                    $galileo->sab = $galileo->sab + $result->getSegment();
                }

            }
			
			
			$galileo->gal = round($galileo->gal * 90 / 100);

            array_push($resultat, $galileo);



        $this->companyService = $this->get('travelport_galileo.service.company');
        $companies = $this->companyService->listPerPage();
        $resultat2 = array();
        foreach ($companies as $company) {
            $results = $this->resultService->getByCompany($company,$begin,$end);
            $galileo = new GalileoResultCompany();
            $galileo->company = $company;
            foreach ($results as $result) {
                if($result->getAgency()->getCodegal() == $pcc){
                    if($result->getOperator()->getCode() == "AMA")
                    {
                        $galileo->ama = $galileo->ama + $result->getSegment();
                    }
                    if($result->getOperator()->getCode() == "GAL")
                    {
                        $galileo->gal = $galileo->gal + $result->getSegment();
                    }
                    if($result->getOperator()->getCode() == "SAB")
                    {
                        $galileo->sab = $galileo->sab + $result->getSegment();
                    }
                }

            }
            $galileo->gal = round($galileo->gal * 90 / 100);
            array_push($resultat2, $galileo);
        }


        return $this->render('TravelportGalileoBundle:Default:report.html.twig', array(
            'resultat' => $resultat,
            'resultat2' => $resultat2,
            'begin' => $begin,
            'end' => $end,
            'results' => $results,
            'agency' => $agency
        ));

    }

   

     /**
    *@Rest\Get("/api/galileo/results/town")
    *@Rest\QueryParam(
    *   name="begin"
    *)
    *@Rest\QueryParam(
    *   name="end"
    *)
    *@Rest\View()
    */
    public function townAction(ParamFetcherInterface $paramFetcher)
    {
        $begin = $paramFetcher->get('begin');
        $end = $paramFetcher->get('end');
        $this->townService = $this->get('travelport_galileo.service.town');
        $this->resultService = $this->get('travelport_galileo.service.result');
        $towns = $this->townService->listPerPage();
        $total=0;
        $gal = 0;
        $ama = 0;
        $sab = 0;
        $resultat = array();
        foreach ($towns as $town) {
            $results = $this->resultService->getByTown($town,$begin,$end);
            $galileo = new GalileoResultTown();
            $galileo->town = $town;
            foreach ($results as $result) {
                $total = $total + $result->getSegment();
                if($result->getOperator()->getCode() == "AMA")
                {
                    $galileo->ama = $galileo->ama + $result->getSegment();
                    $ama = $ama + $result->getSegment();
                }
                if($result->getOperator()->getCode() == "GAL")
                {
                    $galileo->gal = $galileo->gal + $result->getSegment();
                    $gal = $gal + $result->getSegment();
                }
                if($result->getOperator()->getCode() == "SAB")
                {
                    $galileo->sab = $galileo->sab + $result->getSegment();
                    $sab = $sab + $result->getSegment();
                }

            }

            array_push($resultat, $galileo);
        }
        return $this->render('TravelportGalileoBundle:Default:town.html.twig', array(
            'resultat' => $resultat,
            'begin' => $begin,
            'end' => $end,
            'total' => $total,
            'gal' => $gal,
            'ama' => $ama,
            'sab' => $sab
        ));

    }


    /**
    * @Rest\Post("/api/galileo/results/create")
    * @Rest\View(StatusCode = 201))
    */
    public function postAction(Request $request)
    {
        $data = $request->getContent();
         $results = $this->get('jms_serializer')->deserialize($data,'ArrayCollection<Travelport\GalileoBundle\Entity\Result>','json');
         //$results = json_decode($data, true);

         $this->resultService = $this->get('travelport_galileo.service.result');
         $this->operatorService = $this->get('travelport_galileo.service.operator');
         $this->agencyService = $this->get('travelport_galileo.service.agency');
         $this->companyService = $this->get('travelport_galileo.service.company');
         $this->townService = $this->get('travelport_galileo.service.town');

         $resultat = array();
        /* $resultat = $this->resultService->listPerPage();
         if($resultat == null){
            $town = new Town();
            $town = $this->townService->findByCode('XXX');
            if($town == null){
                $town->setCode("XXX");
                $town->setName("VILLE INCONNUE");
                $this->townService->create($town);
            }
            $agencies = $this->agencyService->listPerPage();
            if($agencies == null){
                $this->codeAction($request);
            }
         }*/


         foreach($results as $result)
         {
            
            $operator = new Operator();
            $code = $result->getOperator()->getCode();
            $operator = $this->operatorService->findByCode($code);
            $result->setOperator($operator);
            //var_dump($result->getAgency());
            //echo($result->getAgency());
            //echo 'console.log('. json_encode( $result->getAgency() ) ;
            $agency = new Agency();
             if($operator->getCode()=='AMA'){
                $agency = $this->agencyService->findByCodeama($result->getAgency()->getCodeama());
             }
              if($operator->getCode()=='GAL'){
                $agency = $this->agencyService->findByCodegal($result->getAgency()->getCodeama());
             }
              if($operator->getCode()=='SAB'){
                $agency = $this->agencyService->findByCodesab($result->getAgency()->getCodeama());
             }

             if($agency == null){
                $agency = $this->agencyService->findAgency($result->getAgency()->getCodegal());
             }

              //var_dump($agency);

             if($agency == null){
                //var_dump($result->getAgency()->getTown()->getName());
                $town = new Town();
                $town = $this->townService->findByCode('XXX');
                $agency = new Agency();
                $agency->setTown($town);
                $agency->setName($result->getAgency()->getName());
                $agency->setLocality("XXX");
                if($operator->getCode()=='AMA'){
                    $agency->setCodeama($result->getAgency()->getCodegal());
                    $agency->setCodegal("XXX");
                    $agency->setCodesab("XXX");
                 }
                  if($operator->getCode()=='GAL'){
                    $agency->setCodegal($result->getAgency()->getCodegal());
                     $agency->setCodeama("XXX");
                    $agency->setCodesab("XXX");
                 }
                  if($operator->getCode()=='SAB'){
                    $agency->setCodesab($result->getAgency()->getCodegal());
                     $agency->setCodeama("XXX");
                    $agency->setCodegal("XXX");
                 }

                $agency = $this->agencyService->create($agency);
             }

             $result->setAgency($agency);
             
             $company = $this->companyService->findByCode($result->getCompany()->getCode());
             if($company == null){
                $company = new Company();
                $company->setCode($result->getCompany()->getCode());
                $company->setName($result->getCompany()->getName());
                $company->addAgency($result->getAgency());
                $company = $this->companyService->create($company);
             }

             $result->setCompany($company); 

             //$result->date = new DateTime($result->date); 

              $result = $this->resultService->create($result);

           
        }
         
         $data = $this->get('jms_serializer')->serialize($resultat, 'json');
         $response = new Response($data);
         $response->headers->set('Content-Type', 'application/json');
         return $response;

    }


    /**
    * @Rest\Post("/api/galileo/codes/create")
    * @Rest\View(StatusCode = 201))
    */
    public function codeAction(Request $request)
    {
        $data = $request->getContent();
         $results = $this->get('jms_serializer')->deserialize($data,'ArrayCollection<Travelport\GalileoBundle\Entity\Result>','json');
         //$results = json_decode($data, true);

         $this->resultService = $this->get('travelport_galileo.service.result');
         $this->operatorService = $this->get('travelport_galileo.service.operator');
         $this->agencyService = $this->get('travelport_galileo.service.agency');
         $this->companyService = $this->get('travelport_galileo.service.company');
         $this->townService = $this->get('travelport_galileo.service.town');

        


         foreach($results as $result)
         {
            
             $agency = new Agency();
             $agency = $this->agencyService->findByNameAndTown($result->getAgency()->getName(), $result->getAgency()->getTown()->getCode());
            
                 if($result->getOperator()->getCode() == "AMA"){
                    if($result->getAgency()->getCodegal() != ""){
                        $agency->setCodeama($result->getAgency()->getCodegal());
                    }
                }

                 if($result->getOperator()->getCode() == "GAL"){
                    if($result->getAgency()->getCodegal() != ""){
                       $agency->setCodegal($result->getAgency()->getCodegal());
                    }
                }

                if($result->getOperator()->getCode() == "SAB"){
                    if($result->getAgency()->getCodegal() != ""){
                    $agency->setCodesab($result->getAgency()->getCodegal());
                    }
                }


                $agency = $this->agencyService->update($agency);
             


               
           
        }
         
         //$result = $this->resultService->create($result);
         $data = $this->get('jms_serializer')->serialize($results, 'json');
         $response = new Response($data);
         //$response‐>headers‐>set('Content‐Type', 'application/json');
         $response->headers->set('Content-Type', 'application/json');
         return $response;
    }

}
