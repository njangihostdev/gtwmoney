<?php

namespace Travelport\GalileoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Travelport\GalileoBundle\Service\ResultService;
use Travelport\GalileoBundle\Entity\Result;
use Travelport\GalileoBundle\Entity\Operator;
use Travelport\GalileoBundle\Entity\Agency;
use Travelport\GalileoBundle\Entity\Company;
use Travelport\GalileoBundle\Entity\Town;
use Travelport\GalileoBundle\Service\OperatorService;
use Travelport\GalileoBundle\Service\AgencyService;
use Travelport\GalileoBundle\Service\CompanyService;
use Travelport\GalileoBundle\Service\TownService;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Doctrine\ORM\QueryBuilder;
use CoreBundle\Exception\RessourceValidationException;
use FOS\RestBundle\Controller\FOSRestController;
use Travelport\GalileoBundle\Classes\GalileoResultAgency;
use Travelport\GalileoBundle\Classes\GalileoResultTown;
use Travelport\GalileoBundle\Classes\GalileoResultCompany;



class MoneyController extends Controller
{

	private $resultService;
	private $operatorService;
	private $agencyService;
	private $companyService;
	private $townService;
    private $contactService;
    private $bookingService;

   

    


    /**
    *@Rest\Get("/api/galileo/money/solde")
    *@Rest\QueryParam(
    *   name="login"
    *)
    *@Rest\QueryParam(
    *   name="password"
    *)
    *@Rest\View()
    */
    public function soldeAction(ParamFetcherInterface $paramFetcher)
    {
        $login = $paramFetcher->get('login');
        $password = $paramFetcher->get('password');
        $this->contactService = $this->get('travelport_galileo.service.contact');
        $user = $this->contactService->getByPseudo($login); 
        if($user->getPassword()== $password){

        }else{
            $user->setName("MOT DE PASSE INCORRECT");
            $user->setSolde(0);
        }       

        return $this->render('TravelportGalileoBundle:Default:solde.html.twig', array(
            'user' => $user
        ));




    }



    /**
    *@Rest\Get("/api/galileo/money/booking")
    *@Rest\QueryParam(
    *   name="login"
    *)
    *@Rest\QueryParam(
    *   name="password"
    *)
    *@Rest\View()
    */
    public function bookingAction(ParamFetcherInterface $paramFetcher)
    {
        $login = $paramFetcher->get('login');
        $password = $paramFetcher->get('password');
        $this->contactService = $this->get('travelport_galileo.service.contact');
        $user = $this->contactService->getByPseudo($login); 
        $booking = array();
        if($user->getPassword()== $password){
            $this->bookingService = $this->get('travelport_galileo.service.booking');
            $booking = $this->bookingService->getByAgent($user);
        }else{
            $user->setName("MOT DE PASSE INCORRECT");
            $user->setSolde(0);
        }       

        return $this->render('TravelportGalileoBundle:Default:booking.html.twig', array(
            'user' => $user,
            'booking'=> $booking
        ));




    }


    
   
    }




