<?php

namespace Travelport\GalileoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('TravelportGalileoBundle:Default:index.html.twig');
    }
}
