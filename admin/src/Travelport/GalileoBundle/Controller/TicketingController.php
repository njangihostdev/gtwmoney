<?php

namespace Travelport\GalileoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Travelport\GalileoBundle\Service\TeamService;
use Travelport\GalileoBundle\Entity\Team;
use Travelport\GalileoBundle\Entity\Ticketing;
use Travelport\GalileoBundle\Entity\Agency;
use Travelport\GalileoBundle\Entity\Company;
use Travelport\GalileoBundle\Entity\Contact;
use Travelport\GalileoBundle\Service\TicketService;
use Travelport\GalileoBundle\Service\AgencyService;
use Travelport\GalileoBundle\Service\CompanyService;
use Travelport\GalileoBundle\Service\TownService;
use Travelport\GalileoBundle\Service\ContactService;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Doctrine\ORM\QueryBuilder;
use CoreBundle\Exception\RessourceValidationException;
use FOS\RestBundle\Controller\FOSRestController;
use Travelport\GalileoBundle\Classes\GalileoResultAgency;
use Travelport\GalileoBundle\Classes\GalileoResultTown;
use Travelport\GalileoBundle\Classes\GalileoResultCompany;



class TicketingController extends Controller
{

	private $resultService;
	private $operatorService;
	private $agencyService;
	private $companyService;
	private $townService;


    /**
    * @Rest\Post("/api/galileo/ticket/create")
    * @Rest\View(StatusCode = 201))
    */
    public function postAction(Request $request)
    {
        $data = $request->getContent();
         $ticket = $this->get('jms_serializer')->deserialize($data,'Travelport\GalileoBundle\Entity\Ticketing','json');
         //$results = json_decode($data, true);

         //Call Services to use

         $this->teamService = $this->get('travelport_galileo.service.team');
         $this->ticketingService = $this->get('travelport_galileo.service.ticket');
         $this->agencyService = $this->get('travelport_galileo.service.agency');
         $this->companyService = $this->get('travelport_galileo.service.company');
         $this->townService = $this->get('travelport_galileo.service.town');


         //Assign Helpdesk
         $helpdesks = $this->teamService->getHelpDesk($ticket->getType());
         $helpdesk = $this->ticketingService->getTeamWithMinTickets($helpdesks);
         $ticket->setHelpdesk($helpdesk);

         //Assign Technical if type ticket is INTERNET
         if($ticket->getType()->getName() == 'INTERNET'){
            $tecnicians = $this->teamService->getTechnicalInTown($ticket->getAgency()->getTown());
            $technician = $this->ticketingService->getTeamWithMinTickets($technicians);
            $ticket->setTechnician($technician);
         }

         //Assign Manager Like Technician if type ticket is Other
         if($ticket->getType()->getName() == 'OTHER'){
            $managers = $this->teamService->getManagerInTown($ticket->getAgency()->getTown());
            $manager = $this->ticketingService->getTeamWithMinTickets($managers);
            $ticket->setTechnician($manager);
         }

         $ticket = $this->ticketingService->create($ticket);

         $data = $this->get('jms_serializer')->serialize($ticket, 'json');

         $response = new Response($data);
         $response->headers->set('Content-Type', 'application/json');
         return $response;

    }


     /**
    *@Rest\Get("/api/galileo/ticket/all")
    *@Rest\View()
    */
    public function allAction(ParamFetcherInterface $paramFetcher)
    {
         $this->ticketingService = $this->get('travelport_galileo.service.ticket');
         $results = $this->ticketingService->findNew();  
         $data = $this->get('jms_serializer')->serialize($results, 'json');
         $response = new Response($data);
         $response->headers->set('Content-Type', 'application/json');
         return $response;
    }


    /**
    *@Rest\Get("/api/galileo/ticket/account")
    *@Rest\QueryParam(
    *   name="email"
    *)
    *@Rest\QueryParam(
    *   name="password"
    *)
    *@Rest\View()
    */
    public function accountAction(ParamFetcherInterface $paramFetcher)
    {
         $auth = true;
         $email = $paramFetcher->get('email');
         $password = $paramFetcher->get('password');
         if($password != 'Travelport17'){
            $auth = false;
         }else{
            $this->teamService = $this->get('travelport_galileo.service.team');
            $team = $this->teamService->findOneByEmail($email);
            if($team == null){
                $auth = false;
            }
         }

         if($auth){
            $results = $team;
         }else{
            
            $this->contactService = $this->get('travelport_galileo.service.contact');
            $contact = $this->contactService->findOneByEmail($email);
            if($contact == null || $contact->getPassword()!=$password){
                $results = null;
            }else{
                $results = $contact;
            }

         }

         $data = $this->get('jms_serializer')->serialize($results, 'json');
         $response = new Response($data);
         $response->headers->set('Content-Type', 'application/json');
         return $response;
    }



    /**
    *@Rest\Get("/api/galileo/ticket/create")
    *@Rest\QueryParam(
    *   name="name"
    *)
    *@Rest\QueryParam(
    *   name="number"
    *)
    *@Rest\QueryParam(
    *   name="email"
    *)
    *@Rest\QueryParam(
    *   name="type"
    *)
    *@Rest\QueryParam(
    *   name="description"
    *)
    *@Rest\QueryParam(
    *   name="pcc"
    *)
    *@Rest\View()
    */
    public function requestAction(ParamFetcherInterface $paramFetcher)
    {
         $name = $paramFetcher->get('name');
         $number = $paramFetcher->get('number');
         $email = $paramFetcher->get('email');
         $type = $paramFetcher->get('type');
         $description = $paramFetcher->get('description');
         $pcc = $paramFetcher->get('pcc');

         $this->ticketingService = $this->get('travelport_galileo.service.ticket');
         $this->agencyService = $this->get('travelport_galileo.service.agency');
         $this->contactService = $this->get('travelport_galileo.service.contact');
         $this->teamService = $this->get('travelport_galileo.service.team');
         $this->typeTicketingService = $this->get('travelport_galileo.service.typeTicketing');
         $this->stateTicketingService = $this->get('travelport_galileo.service.stateTicketing');


         $ticket = new Ticketing();
         $ticket->setName($name);
         $ticket->setNumber($number);
         $ticket->setEmail($email);
         $ticket->setClientDescription($description);
         $agency = $this->agencyService->findByCodegal($pcc);

         if($agency == null){
         }else{
            $ticket->setAgency($agency);
         }

         $type = $this->typeTicketingService->findByName($type);
         $ticket->setType($type);

         $state = $this->stateTicketingService->findByName("NEW");
         $ticket->setState($state);


         //Assign Helpdesk
         $helpdesks = $this->teamService->getHelpDesk($ticket->getType());
         $helpdesk = $this->ticketingService->getTeamWithMinTicketingsH($helpdesks);
         $ticket->setHelpdesk($helpdesk);

         //Assign Technical if type ticket is INTERNET
         if($ticket->getType()->getName() == 'INTERNET'){
            $technicians = $this->teamService->getTechnicalInTown($ticket->getAgency()->getTown());
            $technician = $this->ticketingService->getTeamWithMinTicketings($technicians);
            $ticket->setTechnician($technician);
            $state = $this->stateTicketingService->findByName("ASSIGNED");
            $ticket->setState($state);
         }

         $agent = $this->contactService->findByNumber($number);
         $ticket->setAgent($agent);
         $ticket = $this->ticketingService->create($ticket);

         $data = $this->get('jms_serializer')->serialize($ticket, 'json');
         $response = new Response($data);
         $response->headers->set('Content-Type', 'application/json');
         return $response;
    }


     /**
    *@Rest\Get("/api/galileo/ticket/assign")
    *@Rest\QueryParam(
    *   name="id"
    *)
    *@Rest\QueryParam(
    *   name="description"
    *)
    *@Rest\View()
    */
    public function assignAction(ParamFetcherInterface $paramFetcher)
    {
         $id = $paramFetcher->get('id');
         $description = $paramFetcher->get('description');
         $this->ticketingService = $this->get('travelport_galileo.service.ticket');
         $this->agencyService = $this->get('travelport_galileo.service.agency');
         $this->teamService = $this->get('travelport_galileo.service.team');
         $this->typeTicketingService = $this->get('travelport_galileo.service.typeTicketing');
         $this->stateTicketingService = $this->get('travelport_galileo.service.stateTicketing');
         //get ticket
         $ticket = $this->ticketingService->get($id);
         $technicians = $this->teamService->getTechnicalInTown($ticket->getAgency()->getTown());
         $technician = $this->ticketingService->getTeamWithMinTicketings($technicians);
         $ticket->setTechnician($technician);
         $state = $this->stateTicketingService->findByName("ASSIGNED");
         $ticket->setState($state);
         $time = new \Datetime();
         $ticket->setAssignDate($time);
         $ticket->setFormalDescription($description);
         $ticket = $this->ticketingService->update($ticket);

         $data = $this->get('jms_serializer')->serialize($ticket, 'json');
         $response = new Response($data);
         $response->headers->set('Content-Type', 'application/json');
         return $response;
    }


     /**
    *@Rest\Get("/api/galileo/ticket/close")
    *@Rest\QueryParam(
    *   name="id"
    *)
    *@Rest\View()
    */
    public function closeAction(ParamFetcherInterface $paramFetcher)
    {
         $id = $paramFetcher->get('id');
         $this->ticketingService = $this->get('travelport_galileo.service.ticket');
         $this->agencyService = $this->get('travelport_galileo.service.agency');
         $this->teamService = $this->get('travelport_galileo.service.team');
         $this->typeTicketingService = $this->get('travelport_galileo.service.typeTicketing');
         $this->stateTicketingService = $this->get('travelport_galileo.service.stateTicketing');
         //get ticket
         $ticket = $this->ticketingService->get($id);
         $state = $this->stateTicketingService->findByName("CLOSE");
         $ticket->setState($state);
         $time = new \Datetime();
         $ticket->setCloseDate($time);
         $ticket = $this->ticketingService->update($ticket);

         $data = $this->get('jms_serializer')->serialize($ticket, 'json');
         $response = new Response($data);
         $response->headers->set('Content-Type', 'application/json');
         return $response;
    }


     /**
    *@Rest\Get("/api/galileo/ticket/ok")
    *@Rest\QueryParam(
    *   name="id"
    *)
    *@Rest\View()
    */
    public function okAction(ParamFetcherInterface $paramFetcher)
    {
         $id = $paramFetcher->get('id');
         $this->ticketingService = $this->get('travelport_galileo.service.ticket');
         $this->agencyService = $this->get('travelport_galileo.service.agency');
         $this->teamService = $this->get('travelport_galileo.service.team');
         $this->typeTicketingService = $this->get('travelport_galileo.service.typeTicketing');
         $this->stateTicketingService = $this->get('travelport_galileo.service.stateTicketing');
         //get ticket
         $ticket = $this->ticketingService->get($id);
         $ticket->setSatisfaction(true);
         $ticket = $this->ticketingService->update($ticket);

         $data = $this->get('jms_serializer')->serialize($ticket, 'json');
         $response = new Response($data);
         $response->headers->set('Content-Type', 'application/json');
         return $response;
    }


     /**
    *@Rest\Get("/api/galileo/ticket/ko")
    *@Rest\QueryParam(
    *   name="id"
    *)
    *@Rest\View()
    */
    public function koAction(ParamFetcherInterface $paramFetcher)
    {
         $id = $paramFetcher->get('id');
         $this->ticketingService = $this->get('travelport_galileo.service.ticket');
         $this->agencyService = $this->get('travelport_galileo.service.agency');
         $this->teamService = $this->get('travelport_galileo.service.team');
         $this->typeTicketingService = $this->get('travelport_galileo.service.typeTicketing');
         $this->stateTicketingService = $this->get('travelport_galileo.service.stateTicketing');
         //get ticket
         $ticket = $this->ticketingService->get($id);
         $ticket->setSatisfaction(false);
         $ticket = $this->ticketingService->update($ticket);

         $data = $this->get('jms_serializer')->serialize($ticket, 'json');
         $response = new Response($data);
         $response->headers->set('Content-Type', 'application/json');
         return $response;
    }


     /**
    *@Rest\Get("/api/galileo/ticket/me")
    *@Rest\QueryParam(
    *   name="id"
    *)
    *@Rest\View()
    */
    public function meAction(ParamFetcherInterface $paramFetcher)
    {
         $id = $paramFetcher->get('id');
         $this->ticketingService = $this->get('travelport_galileo.service.ticket');
         $this->agencyService = $this->get('travelport_galileo.service.agency');
         $this->teamService = $this->get('travelport_galileo.service.team');
         $this->typeTicketingService = $this->get('travelport_galileo.service.typeTicketing');
         $this->stateTicketingService = $this->get('travelport_galileo.service.stateTicketing');
         $this->contactService = $this->get('travelport_galileo.service.contact');

         //get ticket
         $contact = $this->contactService->get($id);
         $tickets = $contact->getTicketings();

         $results = array();

         foreach ($tickets as $ticket) {
             if($ticket->getSatisfaction()==null){
                array_push($results, $ticket);
             }
         }

         $data = $this->get('jms_serializer')->serialize($results, 'json');
         $response = new Response($data);
         $response->headers->set('Content-Type', 'application/json');
         return $response;
    }


     /**
    *@Rest\Get("/api/galileo/ticket/cancel")
    *@Rest\QueryParam(
    *   name="id"
    *)
    *@Rest\View()
    */
    public function cancelAction(ParamFetcherInterface $paramFetcher)
    {
         $id = $paramFetcher->get('id');
         $this->ticketingService = $this->get('travelport_galileo.service.ticket');
         $this->agencyService = $this->get('travelport_galileo.service.agency');
         $this->teamService = $this->get('travelport_galileo.service.team');
         $this->typeTicketingService = $this->get('travelport_galileo.service.typeTicketing');
         $this->stateTicketingService = $this->get('travelport_galileo.service.stateTicketing');
         //get ticket
         $ticket = $this->ticketingService->get($id);
         $state = $this->stateTicketingService->findByName("CANCEL");
         $ticket->setState($state);
         $time = new \Datetime();
         $ticket->setCloseDate($time);
         $ticket = $this->ticketingService->update($ticket);

         $data = $this->get('jms_serializer')->serialize($ticket, 'json');
         $response = new Response($data);
         $response->headers->set('Content-Type', 'application/json');
         return $response;
    }



    /**
    *@Rest\Get("/api/galileo/contact/create")
    *@Rest\QueryParam(
    *   name="name"
    *)
    *@Rest\QueryParam(
    *   name="number"
    *)
    *@Rest\QueryParam(
    *   name="email"
    *)
    *@Rest\QueryParam(
    *   name="password"
    *)
    *@Rest\QueryParam(
    *   name="pcc"
    *)
    *@Rest\View()
    */
    public function contactAction(ParamFetcherInterface $paramFetcher)
    {
         $name = $paramFetcher->get('name');
         $number = $paramFetcher->get('number');
         $email = $paramFetcher->get('email');
         $pcc = $paramFetcher->get('pcc');
         $password = $paramFetcher->get('password');

         $this->ticketingService = $this->get('travelport_galileo.service.ticket');
         $this->agencyService = $this->get('travelport_galileo.service.agency');
         $this->teamService = $this->get('travelport_galileo.service.team');
         $this->typeTicketingService = $this->get('travelport_galileo.service.typeTicketing');
         $this->contactService = $this->get('travelport_galileo.service.contact');


         $contact = new Contact();
         $contact->setName($name);
         $contact->setContacts($number);
         $contact->setEmail($email);
         $contact->setPassword($password);
         $agency = $this->agencyService->findByCodegal($pcc);

         if($agency == null){

         }else{
            $contact->setAgency($agency);
         }
    
         $contact = $this->contactService->create($contact);

         $data = $this->get('jms_serializer')->serialize($contact, 'json');
         $response = new Response($data);
         $response->headers->set('Content-Type', 'application/json');
         return $response;
    }




   
}
