<?php

namespace Travelport\GalileoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Travelport\GalileoBundle\Service\ResultService;
use Travelport\GalileoBundle\Entity\Result;
use Travelport\GalileoBundle\Entity\Operator;
use Travelport\GalileoBundle\Entity\Agency;
use Travelport\GalileoBundle\Entity\Company;
use Travelport\GalileoBundle\Entity\Town;
use Travelport\GalileoBundle\Service\OperatorService;
use Travelport\GalileoBundle\Service\AgencyService;
use Travelport\GalileoBundle\Service\CompanyService;
use Travelport\GalileoBundle\Service\TownService;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Doctrine\ORM\QueryBuilder;
use CoreBundle\Exception\RessourceValidationException;
use FOS\RestBundle\Controller\FOSRestController;
use Travelport\GalileoBundle\Classes\GalileoResultAgency;
use Travelport\GalileoBundle\Classes\GalileoResultTown;
use Travelport\GalileoBundle\Classes\GalileoResultCompany;



class AgencyController extends Controller
{

	private $resultService;
	private $operatorService;
	private $agencyService;
	private $companyService;
	private $townService;

   
    /**
    *@Rest\Get("/api/galileo/rest/agency")
     *@Rest\QueryParam(
    *   name="page"
    *)
    *@Rest\QueryParam(
    *   name="limit"
    *)
    *@Rest\View()
    */
    public function restAction(ParamFetcherInterface $paramFetcher)
    {
         $page = $paramFetcher->get('page');
         $limit = $paramFetcher->get('limit');
         $this->agencyService = $this->get('travelport_galileo.service.agency');
         $results = $this->agencyService->rest();    
         $data = $this->get('jms_serializer')->serialize($results, 'json');
         $response = new Response($data);
         //$response‐>headers‐>set('Content‐Type', 'application/json');
         $response->headers->set('Content-Type', 'application/json');
         return $response;
    }


    /**
    *@Rest\Get("/api/galileo/agency/fusion")
     *@Rest\QueryParam(
    *   name="pcc"
    *)
    *@Rest\QueryParam(
    *   name="ama"
    *)
    *@Rest\QueryParam(
    *   name="sab"
    *)
    *@Rest\View()
    */
    public function fusionAction(ParamFetcherInterface $paramFetcher)
    {
         $pcc = $paramFetcher->get('pcc');
         $ama = $paramFetcher->get('ama');
         $sab = $paramFetcher->get('sab');
         $this->agencyService = $this->get('travelport_galileo.service.agency');
         $this->resultService = $this->get('travelport_galileo.service.result');
         
         $agency = $this->agencyService->findByCodegal($pcc);

         $agency_ama = $this->agencyService->findByCodeama($ama);
         $agency_sab = $this->agencyService->findByCodesab($sab);

         if($agency_ama != null){
             $agency->setCodeama($agency_ama->getCodeama());
             $results = $agency_ama->getResults();
             foreach ($results as $res) {
                 $res->setAgency($agency);
             }
             $this->agencyService->delete($agency_ama->getId());
         }

         if($agency_sab != null){
             $agency->setCodesab($agency_sab->getCodesab());
             $results = $agency_sab->getResults();
             foreach ($results as $res) {
                 $res->setAgency($agency);
             }
             $this->agencyService->delete($agency_sab->getId());
         }

         
         $data = $this->get('jms_serializer')->serialize($agency, 'json');
         $response = new Response($data);
         //$response‐>headers‐>set('Content‐Type', 'application/json');
         $response->headers->set('Content-Type', 'application/json');
         return $response;
    }

}
