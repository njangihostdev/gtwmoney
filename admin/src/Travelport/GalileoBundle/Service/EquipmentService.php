<?php

namespace Travelport\GalileoBundle\Service;

use Travelport\GalileoBundle\Entity\Equipment;
use Travelport\GalileoBundle\Repository\EquipmentRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;



class EquipmentService
{

  private $request;
  private $em;

  public function __construct(EntityManager $entityManager)
  {
    $this->request = new Request();
    $this->em = $entityManager;
  }

  public function create(Equipment $equipment)
  {
   
     $this->em->persist($equipment);
     $this->em->flush();

     return $equipment;
  }

  public function update(Equipment $equipment)
  {
    
    $equipmentlast = $this->em->getRepository("TravelportGalileoBundle:Equipment")->find($equipment->getId());
    $equipmentlast = $equipment;
    $this->em->flush();
    return $equipmentlast;
  
  }

  public function get($id)
  {

    $equipment = $this->em->getRepository("TravelportGalileoBundle:Equipment")->find($id);
    if (null === $equipment) {
       return null;
    }else{
      return $equipment;
    }
  }

  public function findByNameAndTown($name,$town)
  {

    $equipment = $this->em->getRepository("TravelportGalileoBundle:Equipment")->findByNameAndTown($name, $town);
    if (null === $equipment) {
       return null;
    }else{
      return $equipment;
    }
  }

  public function findEquipment($code)
  {

    $equipment = $this->em->getRepository("TravelportGalileoBundle:Equipment")->findEquipment($code);
    if (null === $equipment) {
       return null;
    }else{
      return $equipment;
    }
  }

   public function findByCodegal($code)
  {

    $equipment = $this->em->getRepository("TravelportGalileoBundle:Equipment")->findOneByCodegal($code);
    if (null === $equipment) {
       return null;
    }else{
      return $equipment;
    }
  }

   public function findByCodeama($code)
  {

    $equipment = $this->em->getRepository("TravelportGalileoBundle:Equipment")->findOneByCodeama($code);
    if (null === $equipment) {
       return null;
    }else{
      return $equipment;
    }
  }

   public function findByCodesab($code)
  {

    $equipment = $this->em->getRepository("TravelportGalileoBundle:Equipment")->findOneByCodesab($code);
    if (null === $equipment) {
       return null;
    }else{
      return $equipment;
    }
  }

  public function listPerPage()
  {
    $equipments = $this->em->getRepository("TravelportGalileoBundle:Equipment")->findAllOrder();
    if (null === $equipments) {
       return null;
    }else{
      return $equipments;
    }
  }

  public function rest()
  {
    $equipments = $this->em->getRepository("TravelportGalileoBundle:Equipment")->findAllRest();
    if (null === $equipments) {
       return null;
    }else{
      return $equipments;
    }
  }

  

  public function delete($id)
  {

    $equipment = $this->em->getRepository("TravelportGalileoBundle:Equipment")->find($id);
    if (null === $equipment) {
       return null;
    }else{
      $this->em->remove($equipment);
      $this->em->flush();
      return $equipment;
    }
  }

 
}
