<?php

namespace Travelport\GalileoBundle\Service;

use Travelport\GalileoBundle\Entity\Result;
use Travelport\GalileoBundle\Repository\ResultRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;



class ResultService
{

  private $request;
  private $em;

  public function __construct(EntityManager $entityManager)
  {
    $this->request = new Request();
    $this->em = $entityManager;
  }

  public function create(Result $result)
  {
   //if ($this->getRequest()->isMethod('POST'))  {
     $this->em->persist($result);
     $this->em->flush();
   //}

     return $result;
  }

  public function update(Result $result)
  {
    //if ($this->getRequest()->isMethod('POST'))  {
    //  $this->em->persist($result);

    $resultlast = $this->em->getRepository("TravelportGalileoBundle:Result")->find($result->getId());
    $resultlast->setSegment($result->getSegment());
    $this->em->flush();
    //}

      return $result;
  }

  public function get($id)
  {

    $result = $this->em->getRepository("TravelportGalileoBundle:Result")->find($id);
    if (null === $result) {
       return null;
    }else{
      return $result;
    }
  }

  public function listPerPage()
  {
    $results = $this->em->getRepository("TravelportGalileoBundle:Result")->findAllOrder();
    if (null === $results) {
       return null;
    }else{
      return $results;
    }
  }

  public function getByAgency($agency,$begin,$end)
  {
    $results = $this->em->getRepository("TravelportGalileoBundle:Result")->getByAgency($agency->getId(),new \Datetime($begin), new \Datetime($end));
    if (null === $results) {
       return null;
    }else{
      return $results;
    }
  }

  public function getByCompany($company,$begin,$end)
  {
    $results = $this->em->getRepository("TravelportGalileoBundle:Result")->getByCompany($company->getId(),new \Datetime($begin), new \Datetime($end));
    if (null === $results) {
       return null;
    }else{
      return $results;
    }
  }

  public function getByTown($town,$begin,$end)
  {
    $results = $this->em->getRepository("TravelportGalileoBundle:Result")->getByTown($town->getId(),new \Datetime($begin), new \Datetime($end));
    if (null === $results) {
       return null;
    }else{
      return $results;
    }
  }

  public function getByDate($date)
  {
    $results = $this->em->getRepository("TravelportGalileoBundle:Result")->findByDate(new \Datetime($date));
    if (null === $results) {
       return null;
    }else{
      return $results;
    }
  }

  public function same($codeOperator,$nameAgency,$townAgency,$codeCompany,$date)
  {
    $results = $this->em->getRepository("TravelportGalileoBundle:Result")->same($codeOperator,$nameAgency,$townAgency,$codeCompany,$date);
    if (null === $results) {
       return null;
    }else{
      return $results;
    }
  }

  public function delete($id)
  {

    $result = $this->em->getRepository("TravelportGalileoBundle:Result")->find($id);
    if (null === $result) {
       return null;
    }else{
      $this->em->remove($result);
      $this->em->flush();
      return $result;
    }
  }

 
}
