<?php

namespace Travelport\GalileoBundle\Service;

use Travelport\GalileoBundle\Entity\Agency;
use Travelport\GalileoBundle\Repository\AgencyRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;



class AgencyService
{

  private $request;
  private $em;

  public function __construct(EntityManager $entityManager)
  {
    $this->request = new Request();
    $this->em = $entityManager;
  }

  public function create(Agency $agency)
  {
   
     $this->em->persist($agency);
     $this->em->flush();

     return $agency;
  }

  public function update(Agency $agency)
  {
    
    $agencylast = $this->em->getRepository("TravelportGalileoBundle:Agency")->find($agency->getId());
    $agencylast->setName($agency->getName());
    $agencylast->setCodegal($agency->getCodegal());
    $agencylast->setCodeama($agency->getCodeama());
    $agencylast->setCodesab($agency->getCodesab());
    $this->em->flush();
    return $agencylast;
  
  }

  public function get($id)
  {

    $agency = $this->em->getRepository("TravelportGalileoBundle:Agency")->find($id);
    if (null === $agency) {
       return null;
    }else{
      return $agency;
    }
  }

  public function findByNameAndTown($name,$town)
  {

    $agency = $this->em->getRepository("TravelportGalileoBundle:Agency")->findByNameAndTown($name, $town);
    if (null === $agency) {
       return null;
    }else{
      return $agency;
    }
  }

  public function findAgency($code)
  {

    $agency = $this->em->getRepository("TravelportGalileoBundle:Agency")->findAgency($code);
    if (null === $agency) {
       return null;
    }else{
      return $agency;
    }
  }

   public function findByCodegal($code)
  {

    $agency = $this->em->getRepository("TravelportGalileoBundle:Agency")->findOneByCodegal($code);
    if (null === $agency) {
       return null;
    }else{
      return $agency;
    }
  }

   public function findByCodeama($code)
  {

    $agency = $this->em->getRepository("TravelportGalileoBundle:Agency")->findOneByCodeama($code);
    if (null === $agency) {
       return null;
    }else{
      return $agency;
    }
  }

   public function findByCodesab($code)
  {

    $agency = $this->em->getRepository("TravelportGalileoBundle:Agency")->findOneByCodesab($code);
    if (null === $agency) {
       return null;
    }else{
      return $agency;
    }
  }

  public function listPerPage()
  {
    $agencys = $this->em->getRepository("TravelportGalileoBundle:Agency")->findAllOrder();
    if (null === $agencys) {
       return null;
    }else{
      return $agencys;
    }
  }

  public function rest()
  {
    $agencys = $this->em->getRepository("TravelportGalileoBundle:Agency")->findAllRest();
    if (null === $agencys) {
       return null;
    }else{
      return $agencys;
    }
  }

  

  public function delete($id)
  {

    $agency = $this->em->getRepository("TravelportGalileoBundle:Agency")->find($id);
    if (null === $agency) {
       return null;
    }else{
      $this->em->remove($agency);
      $this->em->flush();
      return $agency;
    }
  }

 
}
