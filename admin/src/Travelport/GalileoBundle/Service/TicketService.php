<?php

namespace Travelport\GalileoBundle\Service;

use Travelport\GalileoBundle\Entity\Ticketing;
use Travelport\GalileoBundle\Repository\TicketingRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;



class TicketService
{

  private $request;
  private $em;

  public function __construct(EntityManager $entityManager)
  {
    $this->request = new Request();
    $this->em = $entityManager;
  }

  public function create(Ticketing $ticket)
  {
   //if ($this->getRequest()->isMethod('POST'))  {
     $this->em->persist($ticket);
     $this->em->flush();
   //}

     return $ticket;
  }

  public function update(Ticketing $ticket)
  {
    //if ($this->getRequest()->isMethod('POST'))  {
    //  $this->em->persist($ticket);

    $ticketlast = $this->em->getRepository("TravelportGalileoBundle:Ticketing")->find($ticket->getId());
    $ticketlast = $ticket;
    $this->em->flush();
    //}

      return $ticket;
  }

  public function get($id)
  {

    $ticket = $this->em->getRepository("TravelportGalileoBundle:Ticketing")->find($id);
    if (null === $ticket) {
       return null;
    }else{
      return $ticket;
    }
  }

  public function findByCode($code)
  {

    $ticket = $this->em->getRepository("TravelportGalileoBundle:Ticketing")->findOneByCode($code);
    if (null === $ticket) {
       return null;
    }else{
      return $ticket;
    }
  }

  public function listPerPage()
  {
    $tickets = $this->em->getRepository("TravelportGalileoBundle:Ticketing")->findAll();
    if (null === $tickets) {
       return null;
    }else{
      return $tickets;
    }
  }

  public function findNew()
  {
    $state = "NEW";
    $tickets = $this->em->getRepository("TravelportGalileoBundle:Ticketing")->findNew($state);
    if (null === $tickets) {
       return null;
    }else{
      return $tickets;
    }
  }

  public function delete($id)
  {

    $ticket = $this->em->getRepository("TravelportGalileoBundle:Ticketing")->find($id);
    if (null === $ticket) {
       return null;
    }else{
      $this->em->remove($ticket);
      $this->em->flush();
      return $ticket;
    }
  }

  public function getTeamWithMinTicketings($teams)
  {

    $keys = array_keys($teams);
    $hd = $teams[$keys[0]];
    $tickets = $this->em->getRepository("TravelportGalileoBundle:Ticketing")->countNewTicketings($hd->getId());
    $min = count($tickets);

    foreach ($teams as $team) {
      $tickets = $this->em->getRepository("TravelportGalileoBundle:Ticketing")->countNewTicketings($team->getId());
      if($min > count($tickets))
      {
        $min = count($tickets);
        $hd = $team;
      }
    }

    return $hd;
      
  }

   public function getTeamWithMinTicketingsH($teams)
  {

    $keys = array_keys($teams);
    $hd = $teams[$keys[0]];
    $tickets = $this->em->getRepository("TravelportGalileoBundle:Ticketing")->countNewTicketingsH($hd->getId());
    $min = count($tickets);

    foreach ($teams as $team) {
      $tickets = $this->em->getRepository("TravelportGalileoBundle:Ticketing")->countNewTicketingsH($team->getId());
      if($min > count($tickets))
      {
        $min = count($tickets);
        $hd = $team;
      }
    }

    return $hd;
      
  }

 
}
