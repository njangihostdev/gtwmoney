<?php

namespace Travelport\GalileoBundle\Service;

use Travelport\GalileoBundle\Entity\Team;
use Travelport\GalileoBundle\Repository\TeamRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;



class TeamService
{

  private $request;
  private $em;

  public function __construct(EntityManager $entityManager)
  {
    $this->request = new Request();
    $this->em = $entityManager;
  }

  public function create(Team $team)
  {
   //if ($this->getRequest()->isMethod('POST'))  {
     $this->em->persist($team);
     $this->em->flush();
   //}

     return $team;
  }

  public function update(Team $team)
  {
    //if ($this->getRequest()->isMethod('POST'))  {
    //  $this->em->persist($team);

    $teamlast = $this->em->getRepository("TravelportGalileoBundle:Team")->find($team->getId());
    $teamlast = $team;
    $this->em->flush();
    //}

      return $team;
  }

  public function get($id)
  {

    $team = $this->em->getRepository("TravelportGalileoBundle:Team")->find($id);
    if (null === $team) {
       return null;
    }else{
      return $team;
    }
  }

  public function findByCode($code)
  {

    $team = $this->em->getRepository("TravelportGalileoBundle:Team")->findOneByCode($code);
    if (null === $team) {
       return null;
    }else{
      return $team;
    }
  }

   public function findOneByEmail($email)
  {

    $team = $this->em->getRepository("TravelportGalileoBundle:Team")->findOneByEmail($email);
    if (null === $team) {
       return null;
    }else{
      return $team;
    }
  }


  

  public function listPerPage()
  {
    $teams = $this->em->getRepository("TravelportGalileoBundle:Team")->findAll();
    if (null === $teams) {
       return null;
    }else{
      return $teams;
    }
  }

  public function delete($id)
  {

    $team = $this->em->getRepository("TravelportGalileoBundle:Team")->find($id);
    if (null === $team) {
       return null;
    }else{
      $this->em->remove($team);
      $this->em->flush();
      return $team;
    }
  }

  public function getHelpDesk($type)
  {
      $teams = $this->em->getRepository("TravelportGalileoBundle:Team")->findWithTicketings();
      $galileo = array();
      if($type->getName()=='GALILEO'){
        foreach ($teams as $team) {
           if($team->getRole()== 'HELPDESK_GALILEO'){
              array_push($galileo, $team);
           }
        }
      }elseif ($type->getName()=='TECHNICAL' || $type->getName()=='INTERNET') {
        foreach ($teams as $team) {
           if($team->getRole()== 'HELPDESK_TECHNICAL'){
              array_push($galileo, $team);
           }
        }
      }else{
        foreach ($teams as $team) {
           if($team->getRole() == 'HELPDESK_TECHNICAL'){
              array_push($galileo, $team);
           }
        }
      }

      return $galileo;
  }

  public function getTechnicalInTown($town)
  {
      $teams = $this->em->getRepository("TravelportGalileoBundle:Team")->getTechnicalInTown($town->getCode());
      return $teams;
  }

  public function getManagerInTown($town)
  {
      $teams = $this->em->getRepository("TravelportGalileoBundle:Team")->getManagerInTown($town->getCode());
      return $teams;
  }

 
}
