<?php

namespace Travelport\GalileoBundle\Service;

use Travelport\GalileoBundle\Entity\Company;
use Travelport\GalileoBundle\Repository\CompanyRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;



class CompanyService
{

  private $request;
  private $em;

  public function __construct(EntityManager $entityManager)
  {
    $this->request = new Request();
    $this->em = $entityManager;
  }

  public function create(Company $company)
  {
   //if ($this->getRequest()->isMethod('POST'))  {
     $this->em->persist($company);
     $this->em->flush();
   //}

     return $company;
  }

  public function update(Company $company)
  {
    //if ($this->getRequest()->isMethod('POST'))  {
    //  $this->em->persist($company);

    $companylast = $this->em->getRepository("TravelportGalileoBundle:Company")->find($company->id);
    $companylast = $company;
    $this->em->flush();
    //}

      return $company;
  }

  public function get($id)
  {

    $company = $this->em->getRepository("TravelportGalileoBundle:Company")->find($id);
    if (null === $company) {
       return null;
    }else{
      return $company;
    }
  }

  public function findByCode($code)
  {

    $company = $this->em->getRepository("TravelportGalileoBundle:Company")->findOneByCode($code);
    if (null === $company) {
       return null;
    }else{
      return $company;
    }
  }

  public function listPerPage()
  {
    $companys = $this->em->getRepository("TravelportGalileoBundle:Company")->findAllOrder();
    if (null === $companys) {
       return null;
    }else{
      return $companys;
    }
  }

  public function delete($id)
  {

    $company = $this->em->getRepository("TravelportGalileoBundle:Company")->find($id);
    if (null === $company) {
       return null;
    }else{
      $this->em->remove($company);
      $this->em->flush();
      return $company;
    }
  }

 
}
