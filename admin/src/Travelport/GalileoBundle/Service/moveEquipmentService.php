<?php

namespace Travelport\GalileoBundle\Service;

use Travelport\GalileoBundle\Entity\moveEquipment;
use Travelport\GalileoBundle\Repository\moveEquipmentRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;



class moveEquipmentService
{

  private $request;
  private $em;

  public function __construct(EntityManager $entityManager)
  {
    $this->request = new Request();
    $this->em = $entityManager;
  }

  public function create(moveEquipment $moveEquipment)
  {
   //if ($this->getRequest()->isMethod('POST'))  {
     $this->em->persist($moveEquipment);
     $this->em->flush();
   //}

     return $moveEquipment;
  }

  public function update(moveEquipment $moveEquipment)
  {
    //if ($this->getRequest()->isMethod('POST'))  {
    //  $this->em->persist($moveEquipment);

    $moveEquipmentlast = $this->em->getRepository("TravelportGalileoBundle:moveEquipment")->find($moveEquipment->getId());
    $moveEquipmentlast = $moveEquipment;
    $this->em->flush();
    //}

      return $moveEquipment;
  }

  public function get($id)
  {

    $moveEquipment = $this->em->getRepository("TravelportGalileoBundle:moveEquipment")->find($id);
    if (null === $moveEquipment) {
       return null;
    }else{
      return $moveEquipment;
    }
  }

  public function findByCode($code)
  {

    $moveEquipment = $this->em->getRepository("TravelportGalileoBundle:moveEquipment")->findOneByCode($code);
    if (null === $moveEquipment) {
       return null;
    }else{
      return $moveEquipment;
    }
  }

  public function listPerPage()
  {
    $moveEquipments = $this->em->getRepository("TravelportGalileoBundle:moveEquipment")->findAll();
    if (null === $moveEquipments) {
       return null;
    }else{
      return $moveEquipments;
    }
  }

  public function delete($id)
  {

    $moveEquipment = $this->em->getRepository("TravelportGalileoBundle:moveEquipment")->find($id);
    if (null === $moveEquipment) {
       return null;
    }else{
      $this->em->remove($moveEquipment);
      $this->em->flush();
      return $moveEquipment;
    }
  }

 
}
