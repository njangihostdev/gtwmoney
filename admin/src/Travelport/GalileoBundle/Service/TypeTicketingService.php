<?php

namespace Travelport\GalileoBundle\Service;

use Travelport\GalileoBundle\Entity\TypeTicketing;
use Travelport\GalileoBundle\Repository\TypeTicketingRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;



class TypeTicketingService
{

  private $request;
  private $em;

  public function __construct(EntityManager $entityManager)
  {
    $this->request = new Request();
    $this->em = $entityManager;
  }

  public function create(TypeTicketing $typeTicketing)
  {
   //if ($this->getRequest()->isMethod('POST'))  {
     $this->em->persist($typeTicketing);
     $this->em->flush();
   //}

     return $typeTicketing;
  }

  public function update(TypeTicketing $typeTicketing)
  {
    //if ($this->getRequest()->isMethod('POST'))  {
    //  $this->em->persist($typeTicketing);

    $typeTicketinglast = $this->em->getRepository("TravelportGalileoBundle:TypeTicketing")->find($typeTicketing->getId());
    $typeTicketinglast = $typeTicketing;
    $this->em->flush();
    //}

      return $typeTicketing;
  }

  public function get($id)
  {

    $typeTicketing = $this->em->getRepository("TravelportGalileoBundle:TypeTicketing")->find($id);
    if (null === $typeTicketing) {
       return null;
    }else{
      return $typeTicketing;
    }
  }

  public function findByName($name)
  {

    $typeTicketing = $this->em->getRepository("TravelportGalileoBundle:TypeTicketing")->findOneByName($name);
    if (null === $typeTicketing) {
       return null;
    }else{
      return $typeTicketing;
    }
  }

  public function listPerPage()
  {
    $typeTicketings = $this->em->getRepository("TravelportGalileoBundle:TypeTicketing")->findAll();
    if (null === $typeTicketings) {
       return null;
    }else{
      return $typeTicketings;
    }
  }

  public function delete($id)
  {

    $typeTicketing = $this->em->getRepository("TravelportGalileoBundle:TypeTicketing")->find($id);
    if (null === $typeTicketing) {
       return null;
    }else{
      $this->em->remove($typeTicketing);
      $this->em->flush();
      return $typeTicketing;
    }
  }

 
}
