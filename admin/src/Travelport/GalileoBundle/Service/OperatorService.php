<?php

namespace Travelport\GalileoBundle\Service;

use Travelport\GalileoBundle\Entity\Operator;
use Travelport\GalileoBundle\Repository\OperatorRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;



class OperatorService
{

  private $request;
  private $em;

  public function __construct(EntityManager $entityManager)
  {
    $this->request = new Request();
    $this->em = $entityManager;
  }

  public function create(Operator $operator)
  {
   //if ($this->getRequest()->isMethod('POST'))  {
     $this->em->persist($operator);
     $this->em->flush();
   //}

     return $operator;
  }

  public function update(Operator $operator)
  {
    //if ($this->getRequest()->isMethod('POST'))  {
    //  $this->em->persist($operator);

    $operatorlast = $this->em->getRepository("TravelportGalileoBundle:Operator")->find($operator->id);
    $operatorlast = $operator;
    $this->em->flush();
    //}

      return $operator;
  }

  public function get($id)
  {

    $operator = $this->em->getRepository("TravelportGalileoBundle:Operator")->find($id);
    if (null == $operator) {
       return null;
    }else{
      return $operator;
    }
  }

  public function findByCode($code)
  {
    $operator = $this->em->getRepository("TravelportGalileoBundle:Operator")->findOneByCode($code);
    if (null === $operator) {
       return null;
    }else{
      return $operator;
    }
  }


  public function listPerPage()
  {
    $operators = $this->em->getRepository("TravelportGalileoBundle:Operator")->findAll();
    if (null === $operators) {
       return null;
    }else{
      return $operators;
    }
  }

  public function delete($id)
  {

    $operator = $this->em->getRepository("TravelportGalileoBundle:Operator")->find($id);
    if (null === $operator) {
       return null;
    }else{
      $this->em->remove($operator);
      $this->em->flush();
      return $operator;
    }
  }

 
}
