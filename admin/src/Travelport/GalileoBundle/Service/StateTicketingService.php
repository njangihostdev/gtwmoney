<?php

namespace Travelport\GalileoBundle\Service;

use Travelport\GalileoBundle\Entity\StateTicketing;
use Travelport\GalileoBundle\Repository\StateTicketingRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;



class StateTicketingService
{

  private $request;
  private $em;

  public function __construct(EntityManager $entityManager)
  {
    $this->request = new Request();
    $this->em = $entityManager;
  }

  public function create(StateTicketing $stateTicketing)
  {
   //if ($this->getRequest()->isMethod('POST'))  {
     $this->em->persist($stateTicketing);
     $this->em->flush();
   //}

     return $stateTicketing;
  }

  public function update(StateTicketing $stateTicketing)
  {
    //if ($this->getRequest()->isMethod('POST'))  {
    //  $this->em->persist($stateTicketing);

    $stateTicketinglast = $this->em->getRepository("TravelportGalileoBundle:StateTicketing")->find($stateTicketing->getId());
    $stateTicketinglast = $stateTicketing;
    $this->em->flush();
    //}

      return $stateTicketing;
  }

  public function get($id)
  {

    $stateTicketing = $this->em->getRepository("TravelportGalileoBundle:StateTicketing")->find($id);
    if (null === $stateTicketing) {
       return null;
    }else{
      return $stateTicketing;
    }
  }

  public function findByName($name)
  {

    $stateTicketing = $this->em->getRepository("TravelportGalileoBundle:StateTicketing")->findOneByName($name);
    if (null === $stateTicketing) {
       return null;
    }else{
      return $stateTicketing;
    }
  }

  public function listPerPage()
  {
    $stateTicketings = $this->em->getRepository("TravelportGalileoBundle:StateTicketing")->findAll();
    if (null === $stateTicketings) {
       return null;
    }else{
      return $stateTicketings;
    }
  }

  public function delete($id)
  {

    $stateTicketing = $this->em->getRepository("TravelportGalileoBundle:StateTicketing")->find($id);
    if (null === $stateTicketing) {
       return null;
    }else{
      $this->em->remove($stateTicketing);
      $this->em->flush();
      return $stateTicketing;
    }
  }

 
}
