<?php

namespace Travelport\GalileoBundle\Service;

use Travelport\GalileoBundle\Entity\Town;
use Travelport\GalileoBundle\Repository\TownRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;



class TownService
{

  private $request;
  private $em;

  public function __construct(EntityManager $entityManager)
  {
    $this->request = new Request();
    $this->em = $entityManager;
  }

  public function create(Town $town)
  {
   //if ($this->getRequest()->isMethod('POST'))  {
     $this->em->persist($town);
     $this->em->flush();
   //}

     return $town;
  }

  public function update(Town $town)
  {
    //if ($this->getRequest()->isMethod('POST'))  {
    //  $this->em->persist($town);

    $townlast = $this->em->getRepository("TravelportGalileoBundle:Town")->find($town->getId());
    $townlast = $town;
    $this->em->flush();
    //}

      return $town;
  }

  public function get($id)
  {

    $town = $this->em->getRepository("TravelportGalileoBundle:Town")->find($id);
    if (null === $town) {
       return null;
    }else{
      return $town;
    }
  }

  public function findByCode($code)
  {

    $town = $this->em->getRepository("TravelportGalileoBundle:Town")->findOneByCode($code);
    if (null === $town) {
       return null;
    }else{
      return $town;
    }
  }

  public function listPerPage()
  {
    $towns = $this->em->getRepository("TravelportGalileoBundle:Town")->findAll();
    if (null === $towns) {
       return null;
    }else{
      return $towns;
    }
  }

  public function delete($id)
  {

    $town = $this->em->getRepository("TravelportGalileoBundle:Town")->find($id);
    if (null === $town) {
       return null;
    }else{
      $this->em->remove($town);
      $this->em->flush();
      return $town;
    }
  }

 
}
