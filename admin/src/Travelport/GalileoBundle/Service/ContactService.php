<?php

namespace Travelport\GalileoBundle\Service;

use Travelport\GalileoBundle\Entity\Contact;
use Travelport\GalileoBundle\Repository\ContactRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;



class ContactService
{

  private $request;
  private $em;

  public function __construct(EntityManager $entityManager)
  {
    $this->request = new Request();
    $this->em = $entityManager;
  }

  public function create(Contact $contact)
  {
   //if ($this->getRequest()->isMethod('POST'))  {
     $this->em->persist($contact);
     $this->em->flush();
   //}

     return $contact;
  }

  public function update(Contact $contact)
  {
    //if ($this->getRequest()->isMethod('POST'))  {
    //  $this->em->persist($contact);

    $contactlast = $this->em->getRepository("TravelportGalileoBundle:Contact")->find($contact->getId());
    $contactlast = $contact;
    $this->em->flush();
    //}

      return $contact;
  }

  public function get($id)
  {

    $contact = $this->em->getRepository("TravelportGalileoBundle:Contact")->find($id);
    if (null === $contact) {
       return null;
    }else{
      return $contact;
    }
  }

  public function findByNumber($number)
  {

    $contact = $this->em->getRepository("TravelportGalileoBundle:Contact")->findOneByContacts($number);
    if (null === $contact) {
       return null;
    }else{
      return $contact;
    }
  }

  public function listPerPage()
  {
    $contacts = $this->em->getRepository("TravelportGalileoBundle:Contact")->findAll();
    if (null === $contacts) {
       return null;
    }else{
      return $contacts;
    }
  }

  public function GetByPseudo($login){
     $contacts = $this->em->getRepository("TravelportGalileoBundle:Contact")->findOneByPseudo($login);
    if (null === $contacts) {
       return null;
    }else{
      return $contacts;
    }
  }

  public function delete($id)
  {

    $contact = $this->em->getRepository("TravelportGalileoBundle:Contact")->find($id);
    if (null === $contact) {
       return null;
    }else{
      $this->em->remove($contact);
      $this->em->flush();
      return $contact;
    }
  }

 
}
