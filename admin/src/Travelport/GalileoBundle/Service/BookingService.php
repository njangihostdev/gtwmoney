<?php

namespace Travelport\GalileoBundle\Service;

use Travelport\GalileoBundle\Entity\Booking;
use Travelport\GalileoBundle\Repository\BookingRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;



class BookingService
{

  private $request;
  private $em;

  public function __construct(EntityManager $entityManager)
  {
    $this->request = new Request();
    $this->em = $entityManager;
  }

  public function create(Booking $booking)
  {
   
     $this->em->persist($booking);
     $this->em->flush();

     return $booking;
  }

  public function update(Booking $booking)
  {
    
    $bookinglast = $this->em->getRepository("TravelportGalileoBundle:Booking")->find($booking->getId());
    $bookinglast->setName($booking->getName());
    $bookinglast->setCodegal($booking->getCodegal());
    $bookinglast->setCodeama($booking->getCodeama());
    $bookinglast->setCodesab($booking->getCodesab());
    $this->em->flush();
    return $bookinglast;
  
  }

  public function get($id)
  {

    $booking = $this->em->getRepository("TravelportGalileoBundle:Booking")->find($id);
    if (null === $booking) {
       return null;
    }else{
      return $booking;
    }
  }

  public function findByNameAndTown($name,$town)
  {

    $booking = $this->em->getRepository("TravelportGalileoBundle:Booking")->findByNameAndTown($name, $town);
    if (null === $booking) {
       return null;
    }else{
      return $booking;
    }
  }

  public function getByAgent($agent)
  {

    $booking = $this->em->getRepository("TravelportGalileoBundle:Booking")->findBooking($agent->getPseudo());
    if (null === $booking) {
       return null;
    }else{
      return $booking;
    }
  }

   public function findByCodegal($code)
  {

    $booking = $this->em->getRepository("TravelportGalileoBundle:Booking")->findOneByCodegal($code);
    if (null === $booking) {
       return null;
    }else{
      return $booking;
    }
  }

   public function findByCodeama($code)
  {

    $booking = $this->em->getRepository("TravelportGalileoBundle:Booking")->findOneByCodeama($code);
    if (null === $booking) {
       return null;
    }else{
      return $booking;
    }
  }

   public function findByCodesab($code)
  {

    $booking = $this->em->getRepository("TravelportGalileoBundle:Booking")->findOneByCodesab($code);
    if (null === $booking) {
       return null;
    }else{
      return $booking;
    }
  }

  public function listPerPage()
  {
    $bookings = $this->em->getRepository("TravelportGalileoBundle:Booking")->findAllOrder();
    if (null === $bookings) {
       return null;
    }else{
      return $bookings;
    }
  }

  public function rest()
  {
    $bookings = $this->em->getRepository("TravelportGalileoBundle:Booking")->findAllRest();
    if (null === $bookings) {
       return null;
    }else{
      return $bookings;
    }
  }

  

  public function delete($id)
  {

    $booking = $this->em->getRepository("TravelportGalileoBundle:Booking")->find($id);
    if (null === $booking) {
       return null;
    }else{
      $this->em->remove($booking);
      $this->em->flush();
      return $booking;
    }
  }

 
}
